import glob
import sys
import re
import collections
import csv
import json

def entity_map(community):
    d = {}
    for k in community["entities"].keys():
        for index, model_id in enumerate(community["entities"][k]):
            d[model_id] = "{}{}".format(k[0].upper(),index)

    return d

def process_casedir(casedir):
    subdirs = (subdir for subdir in glob.iglob(casedir+'/*') if len(glob.glob(subdir+"/density.txt")) > 0)
    identifier = casedir.split("/")[-2].split("-")
    fragments = int(identifier[0])
    cover = float(identifier[1])

    if len(identifier) > 2:
        simtype = identifier[2]
    else:
        simtype = None

    for subdir in subdirs:
        print subdir
        fname = "{}/density.txt".format(subdir)
        community_fname = "{}/community.json".format(subdir)
        community = json.load(open(community_fname))
        dmap = entity_map(community)

        subidentifier = subdir.split("/")[-1].split("-")
        replicate = subidentifier[0]
        seed = subidentifier[1]

        with open(fname) as tsvfile:
            reader = csv.DictReader(tsvfile, dialect='excel-tab')
            for row in reader:
                nrow = {}
                for (k,v) in row.items():
                    if not k in ["time","events"]:
                        nrow[dmap[int(k)]] = v
                    else:
                        nrow[k] = v
                nrow["landscape.fragments"] = fragments
                nrow["landscape.cover"] = cover
                nrow["replicate.id"] = replicate
                nrow["replicate.seed"] = seed
                if simtype is not None:
                    nrow["simulation.type"] = simtype
                yield nrow

def process(rootdir):
    outdir = "{}/out/".format(rootdir)

    pattern = re.compile(".*/([0-9]+)-(0.[0-9]+)/")
    d = collections.defaultdict(list)
    dirs = []

    for rdir in glob.iglob(outdir+'*/'):
        for r in process_casedir(rdir):
            yield r

rootdir = sys.argv[1]
outcsv = sys.argv[2]

rows = process(rootdir)
first = rows.next()
with open(outcsv, 'w') as f:
    w = csv.DictWriter(f, first.keys())
    w.writeheader()
    w.writerow(first)
    w.writerows(rows)
