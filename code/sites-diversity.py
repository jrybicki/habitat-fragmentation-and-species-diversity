import sloss
import sys
import json
import util
import math
import numpy
import glob
import csv
import argparse

def diversity(order, sites):
    A = numpy.array(sites,dtype=float)
    if order == 1:
        return diversity_shannon(A)
    else:
        return diversity_order(order, A)

def nonzeros(ps): 
    return numpy.array([p for p in ps if p > 0]) 

def diversity_shannon(A):
    N, S = A.shape
    alphalog = 0.0
    for site in A:
        for p in nonzeros(site):
            alphalog += p*numpy.log(p)
    alphalog /=  float(N)

    gammalog = 0.0
    for column in A.T:
        pi = numpy.average(column) # average proportion of species i
        if pi > 0:
            gammalog += pi*numpy.log(pi)
    
    alpha = numpy.exp(-alphalog)
    gamma = numpy.exp(-gammalog)
    return alpha, gamma
    
def diversity_order(order, A):
    N, S = A.shape
    lalpha = sum( ((nonzeros(row)**order).sum() for row in A) ) / float(N)

    gavg = (numpy.average(column) for column in A.T)
    lgamma = sum( (x**order for x in nonzeros(gavg) ) )

    if lalpha == 0 or lgamma == 0:
        return (-1,-1)

    r = 1.0/(1.0-order)
    alpha = lalpha ** r
    gamma = lgamma ** r

    #assert alpha <= gamma
    return alpha, gamma

def fragment_diversity(args, indata, entity_map):
    data = indata["data"][-1]
    areas = numpy.array(sloss.get_fragment_areas(indata) )
    areas /= areas.sum()

    #print areas

    satiated = [sloss.get_type_count("satiated", counts, entity_map) for counts in data["fragment.counts"]]
    sites = []
    for (w, s) in zip(areas,satiated):
        if sum(s) > 0:
            if not args.equal_weights:
                sites.append(w * s / s.sum())
            else:
                sites.append(s / s.sum())

    if len(sites) == 0:
        return

    row = {}
    row['nonempty.fragments'] = len(sites)
    for order in args.orders:
        alpha, gamma = diversity(order, sites)
        beta = gamma / alpha
        row['order'] = order
        row['alpha'] = alpha
        row['beta'] = beta
        row['gamma'] = gamma
        yield row


def site_diversity(args, indata, entity_map):
    sites = indata["data"][-1]["sample.counts"]
    satiated_at_sites = [sloss.get_type_count("satiated", counts, entity_map) for counts in sites]

    if not "sample.sites" in indata:
        return

    # Only consider sites with species
    total_sites = len(satiated_at_sites)
    filtered = [s for s in satiated_at_sites if sum(s) > 0]
    if len(filtered) < 2:
        return

    normalized = [s / float(s.sum()) for s in filtered]

    row = {}
    row['sampling.radius'] = siteradius
    row['total.sites'] = len(sites)
    row['nonempty.sites'] = len(filtered)
    siteradius = indata["sample.window.radius"]

    for order in args.orders:
        alpha, gamma = diversity(order, normalized)
        if (alpha, gamma) == (-1,-1):
            continue

        beta = gamma / alpha
        row['order'] = order
        row['alpha'] = alpha
        row['beta'] = beta
        row['gamma'] = gamma
        yield row

def process_data(args, func):
    rootdir = args.rootdir
    orders = args.orders
    data = sloss.read_data(rootdir)

    for (fragments, cover), replicate_paths in data.items():
        for basedir in replicate_paths:
            community = json.load(util.open_file("{}/community.json".format(basedir)))
            entity_map = sloss.get_entity_map(community)
            for indata_fname in glob.glob("{}/output*json.gz".format(basedir)):
                indata = json.load(util.open_file(indata_fname))
                rid = basedir.split("/")[-1]
                row = {}
                row['landscape.fragments'] = fragments
                row['landscape.cover'] = cover
                row['replicate.id'] = rid

                for r in func(args, indata, entity_map):
                    r.update(row)
                    yield r


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('rootdir', help='rootdir')
    parser.add_argument('output', type=str, default=None)
    parser.add_argument('--orders', type=float, nargs="*", default=[0,1])
    parser.add_argument('--use_sites', action='store_true', help="Sample sites")
    parser.add_argument('--equal_weights', action='store_true', help="Use equal wieghts (or not)")
    parser.set_defaults(use_sites=False)
    parser.set_defaults(equal_weights=True)

    args = parser.parse_args()
    
    func = fragment_diversity
    if args.use_sites:
        func = site_diversity
        print "Computing sample site diversities"
    else:
        print "Computing fragment diversities"

    s = process_data(args, func)
    first = s.next()
    w = csv.DictWriter(open(args.output, 'w'), fieldnames=first.keys())
    w.writeheader()
    w.writerow(first)
    for row in s:
        w.writerow(row)
    
if __name__=='__main__':
    main()


