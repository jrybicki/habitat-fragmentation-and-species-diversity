#!/usr/bin/env python
import sys
import random
import argparse
import time
import json
import copy
import os
import stat

import util
import generate_community
import fragmenter
import combinations

COLLECT_SCRIPT = """python sfar.py {rootdir}/out/ {rootdir}/
python sloss.py {rootdir}/out/ {rootdir}/ 0
./get-focal.sh {rootdir} {seed}
python summarise-density.py {rootdir}/ {rootdir}/density.csv
"""

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument('-U', type=float, required=True, help='UxU domain')
parser.add_argument('-T0','--pretime', type=float, required=True, help='Initial simulation time')
parser.add_argument('-T1','--posttime', type=float, required=True, help='Simulation time after fragmentation')
parser.add_argument('-R', '--replicates', type=int, required=True, help='Number of replicates')
parser.add_argument('-S', '--species', type=int, required=True, help='Number of species')
parser.add_argument('--seed', required=True, help='seed')
parser.add_argument('--directory', required=True, help='Output directory')

parser.add_argument('--predt', default=1.0, help="Delta time for initial simulation")
parser.add_argument('--postdt', default=None, help="Delta time for fragment sims")
parser.add_argument('--skip_init', action='store_true', help="skip initial simulation run")
parser.set_defaults(skip_init=False)

# 
parser.add_argument("--defaults", default=None, help="Default community parameters")

# Analyse parameters (defaults should suffice)
parser.add_argument("--focal_sample_count", default=5, help = "How many sites to sample for focal data")
parser.add_argument("--focal_accuracy", default=100, help = "How many samples to take per unit area when approximating habitat cover")

# -- Scenario parameters --

# Dispersal
parser.add_argument('-d', '--dispersal', type=float, required=True)

# Establishment (hostile/non-hostile matrix)
parser.add_argument('--establishment', dest='establishment', action='store_true')
parser.add_argument('--no-establishment', dest='establishment', action='store_false')
parser.set_defaults(feature=False)

# Gradient (environmental variation in patch types)
parser.add_argument('--gradient', dest='gradient', action='store_true')
parser.add_argument('--no-gradient', dest='gradient', action='store_false')
parser.set_defaults(gradient=False)

parser.add_argument('-H', '--hops', type=int, required=True)

parser.add_argument('--fragparam', help="delimited list of fragmentation variance parameters; e.g. 'cover:0.001,0.1,0.3'", type=str, nargs="*", default=[])
parser.add_argument('--fragtype', type=str, help='fragmentation pattern', default="lognorm")

# Misc parameters
parser.add_argument('--verbosity', default=1, help="Verbosity level of preparation script")

args = parser.parse_args()


if args.postdt is None:
    postdt = args.posttime
else:
    postdt = args.postdt

if args.predt is None:
    predt = args.pretime
else:
    predt = args.predt

def parse_fragmentation_parameters():
    d = {
        "variance" : [1.0],
        "cover" : [0.00125, 0.0025, 0.005, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.48],
        "fragments" : [1, 4, 16, 64, 256, 1024]
    }

    for line in args.fragparam:
        p,ls = line.split(":")
        d[p] = ls.split(",")

    return d

fragmentation_parameters = parse_fragmentation_parameters()


# -- Common filenames --

SIM_PATH = "./simulator/fragment-sim"
ANALYSE_PATH = "./simulator/analyse"
FOCAL_PATH = "./simulator/focal"

INPUT_DIR = args.directory
OUT_DIR = "{}/out/".format(INPUT_DIR)
TMP_DIR = "{}/tmp/".format(INPUT_DIR)
INIT_DIR = "{}/init".format(INPUT_DIR)
BATCH_DIR = "{}/batch/".format(INPUT_DIR)

DIRS_TO_CREATE = [INPUT_DIR, OUT_DIR, TMP_DIR, INIT_DIR,BATCH_DIR]

INIT_COMMUNITY_FILE = "{}/community.json".format(INPUT_DIR)
INIT_SNAPSHOT_FILE = "{}/out.snapshot".format(INIT_DIR)
INIT_DENSITY_FILE = "{}/density.txt".format(INIT_DIR)
INITIAL_STATE_FILE = "{}/initial.state.txt".format(INIT_DIR)
FRAG_FILE = "{}/fragmentation_parameters.json".format(INPUT_DIR)

SIMULATE_FRAGMENTS_BATCH = "{}/simulate-fragments.sh".format(BATCH_DIR)
ANALYSE_BATCH = "{}/postprocess.sh".format(BATCH_DIR)
RUN_ALL_BATCH = "{}/all.sh".format(BATCH_DIR)
COLLECT_BATCH = "{}/collect.sh".format(BATCH_DIR)

SAMPLING_RADII = [1.0, 2.0]

# --- utilities ---

def log(s, level=1):
    if args.verbosity >= level:
        time_diff = time.time() - start_time
        print "[{} {}] {:.2f}\t{}".format(sys.argv[0], INPUT_DIR, time_diff, s)

def new_seed():
    return random.randrange(0, 2**32-1)

# --- Script ---

def init():
    log("Start time: {}".format(time.strftime("%H:%M:%S")))
    log("Scenario directory: '{}".format(INPUT_DIR))

    log(args)
    log("RNG seed '{}'".format(args.seed))
    random.seed(args.seed)
    
    log("Fragmentation pattern parameters:")
    for key, value in fragmentation_parameters.items():
        log("\t {} = {}".format(key,value))

    for d in DIRS_TO_CREATE:
        log("Create directory '{}'".format(d))
        util.new_dir(d)

def init_community():
    log("Generate community data file '{}'".format(INIT_COMMUNITY_FILE))
    if args.defaults is not None:
        generate_community.defaults = json.load(open(args.defaults))

    community = generate_community.generate(
        species_count=args.species, 
        dispersal_scale=args.dispersal, 
        propagule_establishment=args.establishment, 
        hops=args.hops, 
        gradient=args.gradient
    )

    json.dump(fragmentation_parameters, open(FRAG_FILE, 'w'))
    json.dump(community, open(INIT_COMMUNITY_FILE, 'w'))

    return community

def run_simulation():
    if args.skip_init:
        log("Skipping initial simulation")
        return

#    if args.empty_init:
#        log("Using empty initial scenario")
#        with open(INITIAL_STATE_FILE, 'w') as f:
#            f.close()
#        return
    
    log("Run initial simulation")
    sim_str = "./simulator/fragment-sim -U {U} -t {t} -o {snapshot} -m {community} -s {seed} -d {density} --dt {predt} > {initoutput}".format(
        U=args.U,
        t=args.pretime,
        snapshot=INIT_SNAPSHOT_FILE,
        community=INIT_COMMUNITY_FILE,
        seed=new_seed(),
        density=INIT_DENSITY_FILE,
        initoutput="{}/stdout".format(INIT_DIR),
        predt=predt
    )
    log(sim_str)

    retcode = util.run(sim_str)
    if retcode == 0:
        log("Simulation done. Getting snapshot of final state to use as initial state for fragmented scenarios")
    else:
        log("ERROR: Simulation failed with return code {}".format(retcode))
        util.run("cat {}/stdout".format(INIT_DIR))

    map_snapshot_str = "./get-final-state.sh {} > {}".format(INIT_SNAPSHOT_FILE, INITIAL_STATE_FILE)
    log(map_snapshot_str)
    util.run(map_snapshot_str)


def prepare_case(pars, community, index):
    sim_seed = new_seed()
    basedir = "{}/{}-{}/{}-{}".format(OUT_DIR, pars["fragments"], pars["cover"], index, sim_seed)
    input_community_fname = "{}/community.json".format(basedir)
    output_snapshot_fname = "{}/out.snapshot".format(basedir)
    density_output_fname = "{}/density.txt".format(basedir)

    script_fname = "{}/run.sh".format(basedir)
    sim_script_fname = "{}/sim.sh".format(basedir)
    analyse_script_fname = "{}/postprocess.sh".format(basedir)

    # Basic commands
    sim_str_base = "{sim} -U {u} -t {t} --dt {dt} -o {snapshot} -d {dens} -m {input_model} -s {seed} {input} > {initoutput}"
    analyse_str_base = "{analyse} -s {sample_time} -r {sample_radius} -c {input_model} -i {snapshot} > {output}"
    focal_str_base = "{focal} --seed {seed} --accuracy {focal_accuracy} -n {focal_sample_count} -r {minradius} < {analysis_output} > {focal_output}"

    # Create basedir
    util.new_dir(basedir)

    # Generate input community file
    fragments = fragmenter.fragment(args.fragtype, pars)
    input_json = copy.deepcopy(community)
    input_json["fragments"] = [{"x" : f[0][0], "y" : f[0][1], "r" : f[1]} for f in fragments]
    json.dump(input_json, open(input_community_fname, 'w'))


    # Simulation cmd
    sim_str = sim_str_base.format(
            sim = SIM_PATH, 
            u = args.U, 
            dt = postdt, 
            t = args.posttime, 
            snapshot = output_snapshot_fname, 
            input_model = input_community_fname, 
            seed = sim_seed, 
            dens = density_output_fname, 
            input = "-o "+INITIAL_STATE_FILE if not args.skip_init else "",
            initoutput = "{}/stdout".format(basedir)
    )

    with open(sim_script_fname, 'w') as f:
        f.write(sim_str)
        f.write("\n")

    analyse_lines = []

    for radius in SAMPLING_RADII:
        analysis_output_fname = "{}/output-{}.json".format(basedir, radius)
        focal_output_fname = "{}/local-{}.json".format(basedir, radius)

        # Analyse 1/2 
        analyse_str = analyse_str_base.format(
            analyse = ANALYSE_PATH,
            sample_time = args.posttime-1,
            sample_radius = radius,
            snapshot =  output_snapshot_fname, 
            input_model = input_community_fname, 
            output = analysis_output_fname
        )
        analyse_lines.append(analyse_str)


        # Analyse 2/2
        focal_str = focal_str_base.format(
            focal = FOCAL_PATH,
            seed = new_seed(),
            focal_accuracy = args.focal_accuracy,
            focal_sample_count = args.focal_sample_count,
            minradius = radius,
            analysis_output = analysis_output_fname,
            focal_output = focal_output_fname
        )
        analyse_lines.append(focal_str)

        # Compress output
        analyse_lines.append("gzip -f {}".format(analysis_output_fname))
        analyse_lines.append("gzip -f {}".format(focal_output_fname))

    analyse_lines.append("gzip -f {}".format(output_snapshot_fname)) 

    with open(analyse_script_fname, 'w') as f:
        for l in analyse_lines:
            f.write(l)
            f.write("\n")

    with open(script_fname, "w") as f:
        f.write("{}\n".format(sim_script_fname))
        f.write("{}\n".format(analyse_script_fname))

    return basedir, script_fname, sim_script_fname, analyse_script_fname
        

def prepare_replicates(community):
    log("Initialise fragmentation simulation replicates")

    cases = []
    for p in combinations.parameter_combinations(fragmentation_parameters):
        log("Cover: {cover} Fragments: {fragments} Variance: {variance}".format(**p), level=2)
        for i in xrange(args.replicates):
            c = prepare_case(p, community, i)
            cases.append(c)

    return cases

def prepare_batch(cases):
    def chmod_exec(fname):
        st = os.stat(fname)
        os.chmod(fname, st.st_mode | stat.S_IEXEC)

    all_batch = open(RUN_ALL_BATCH, 'w')
    sim_batch = open(SIMULATE_FRAGMENTS_BATCH, 'w')
    analyse_batch = open(ANALYSE_BATCH, 'w')
    
    for c in cases:
        basedir, script_fname, sim_script_fname, analyse_script_fname = c

        chmod_exec(script_fname)
        chmod_exec(sim_script_fname)
        chmod_exec(analyse_script_fname)

        all_batch.write("./{}\n".format(script_fname))
        sim_batch.write("./{}\n".format(sim_script_fname))
        analyse_batch.write("./{}\n".format(analyse_script_fname))

    all_batch.close()
    sim_batch.close()
    analyse_batch.close()

    with open(COLLECT_BATCH, 'w') as f:
        f.write(COLLECT_SCRIPT.format(rootdir = INPUT_DIR, seed=new_seed()))

    chmod_exec(RUN_ALL_BATCH)
    chmod_exec(SIMULATE_FRAGMENTS_BATCH)
    chmod_exec(ANALYSE_BATCH)
    chmod_exec(COLLECT_BATCH)

    log("Run ONLY simulations: {}".format(SIMULATE_FRAGMENTS_BATCH))
    log("Run ONLY analysis/postprocessing: {}".format(ANALYSE_BATCH))
    log("Run BOTH simulations and postprocessing: {}".format(RUN_ALL_BATCH))
    log("Collect data: {}".format(COLLECT_BATCH))

def main():
    init()
    community = init_community()
    run_simulation()
    basedirs = prepare_replicates(community)
    prepare_batch(basedirs)

if __name__ == '__main__':
    main()

