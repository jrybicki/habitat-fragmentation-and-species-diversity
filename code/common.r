# Color schemes
library(RColorBrewer)

sfar_colors <-  c("#d73027","#fc8d59", "#fee090","#7fbf7b","#91bfdb","#4575b4", "#000000")
cover_colors <- rev(brewer.pal(n=8, name="Dark2"))

ll_colors <- c("black", "orange",  "#4575b4")
ll_types <- c("dashed", "solid", "solid", "solid")
ll_sizes <- c(0.5, 1, 0.75, 1)

# Basic theme
get_theme <- function() {
        theme(legend.position='bottom',
              legend.background=element_rect(size=0, fill = "transparent", colour = "transparent"),
              legend.key=element_rect(fill = "transparent", colour = "transparent"),
              legend.key.height = unit(0.6,'lines'),
              legend.key.width = unit(0.8, 'lines'),
              legend.title = element_text(size=8),
              legend.text = element_text(size=7),
              legend.margin = margin(0),
              plot.margin = margin(0),
              panel.border = element_rect(fill=NA,color="black"),
              axis.text=element_text(size=7),
              axis.title=element_text(size=8),
              axis.line = element_blank(),
              strip.background = element_blank(),
              strip.text = element_text(size=8, hjust=0))
}


# Filename processing

get_mode_and_dispersal <- function(fname) {
    d <- dirname(fname)
    sd <- unlist(strsplit(d, "/"))
    t <- sd[length(sd)]
    vals <- unlist(strsplit(t, "-"))
    mode_val <- vals[2]
    delta_val <- as.integer(gsub("d", "", vals[3]))
    list(mode_val=mode_val, delta_val=delta_val)
}

get_long_mode_name <- function(mode_val) {
    if (mode_val == "passive") return("Passive-Habitable");
    if (mode_val == "hostile") return("Passive-Hostile");
    if (mode_val == "active") return("Active-Habitable")
    "Unknown mode";
}

load_data_with_mode <- function(fname) {
    data <- read.csv(fname)
    mode <- get_mode_and_dispersal(fname)
    print(mode)
    data$delta_val <- mode$delta_val
    data$mode_val <- mode$mode_val
    data$mode_val <- get_long_mode_name(mode$mode_val)
#    set_mode_factors(data)
    data
}

set_mode_factors <- function(data) {
    data$delta_val <- paste("delta==",data$delta_val,sep="")
    data$delta_val <- factor(data$delta_val, levels = c("delta== 1", "delta== 3", "delta== 10"))
    data$mode_val <- factor(data$mode_val, levels = c("Passive-Hostile", "Passive-Habitable", "Active-Habitable"))
}

paste_filename_to_subdirs <- function(fname, dirs) {
    mapply(function(x) { paste(x, fname, sep="/") }, dirs)
}


# Read the suite data (9 files: MODE x DISPERSAL)
read_suite_data <- function(files) {
    data <- data.frame()

    i = 1
    for (f in files[1:length(files)]) {
        input_data <- load_data_with_mode(f)
        input_data$case <- as.factor(f)
        print(summary(input_data))
        data <- rbind(data, input_data)
        i <- i + 1
    }

    #data$mode_val <- factor(data$mode_val, levels = c("passive", "hostile", "active"))
    data$mode_val <- factor(data$mode_val, levels = c("Passive-Hostile", "Passive-Habitable", "Active-Habitable"))
    
    set_mode_factors(data)
    data
}
