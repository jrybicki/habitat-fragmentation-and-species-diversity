#!/usr/bin/env python
import sys
import random
import argparse
import time
import json
import copy
import os
import stat

import util

start_time = time.time()

def log(s, level=1):
    if args.verbosity >= level:
        time_diff = time.time() - start_time
        print "[{}] {:.2f}\t{}".format(sys.argv[0], time_diff, s)

parser = argparse.ArgumentParser()
parser.add_argument('--maxjobs', default=1000, type=int)
parser.add_argument('--cputime', default="24:00:00")
parser.add_argument('--cpumem', default="1000")
parser.add_argument('--partition', default="serial")
parser.add_argument('--verbosity', default=1)
parser.add_argument('batch')
parser.add_argument('out')
parser.add_argument('--tmpdir', default=None)

args = parser.parse_args()

tmpdir = "{}-tmp".format(args.out)
if args.tmpdir is not None:
    tmpdir = args.tmpdir


def chmod_exec(fname):
    st = os.stat(fname)
    os.chmod(fname, st.st_mode | stat.S_IEXEC)

with open(args.batch) as f:
    lines = f.readlines()

log("Read {} jobs from '{}'".format(len(lines), args.batch))
log("Creating directory '{}'".format(tmpdir))
util.new_dir(tmpdir)

task_count = len(lines)
job_count = min(task_count, args.maxjobs) # how many jobs are scheduled
tasksperjob = int(task_count / float(job_count) + 0.5) # how many tasks per job
random.shuffle(lines) # random permutation for quick'n'dirty load balancing

log("{} jobs, {} tasks, up to {} tasks per job".format(job_count, task_count, tasksperjob))

job_index = 1
task_index = 0
chunk_fnames = []
while task_index < task_count:
    tasks_until = min(task_index + tasksperjob, task_count)
    chunk_fname = "{}/job-{}.sh".format(tmpdir, job_index)
    log("Writing jobs {}..{} to '{}'".format(task_index, tasks_until, chunk_fname),2)
    with open(chunk_fname, 'w') as f:
        f.writelines(lines[task_index:tasks_until])
        task_index = tasks_until
    job_index += 1
    chmod_exec(chunk_fname)

slurm_fname = args.out
slurm_script = """#!/bin/bash -l
#SBATCH -J array_job
#SBATCH -e {slurmdir}/array_job_err_%A_%a.txt
#SBATCH -o {slurmdir}/array_job_out_%A_%a.txt
#SBATCH -t {cputime}
#SBATCH --mem-per-cpu={cpumem}
#SBATCH --array=1-{lines}
#SBATCH -n 1
#SBATCH -p {partition}
./{slurmdir}/job-$SLURM_ARRAY_TASK_ID.sh
""".format(
        slurmdir = tmpdir,
        cputime = args.cputime,
        cpumem = args.cpumem,
        partition = args.partition,
        lines = job_count
)

log("Writing SLURM script to '{}'".format(slurm_fname))
with open(slurm_fname, 'w') as f:
    f.write(slurm_script)
