#!/usr/bin/env python
import sys
import random
import argparse
import time
import json
import copy
import os
import stat

import util
import generate_community
import fragmenter
import combinations
import setup

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument('-U', type=float,
                    required=True,
                    help='UxU domain')

parser.add_argument('-T','--time',
                    type=float,
                    required=True,
                    help='Simulation time')

parser.add_argument('-R', '--replicates',
                    type=int,
                    required=True,
                    help='Number of replicates')

parser.add_argument('--seed',
                    required=True,
                    help='seed')

parser.add_argument('--directory',
                    required=True,
                    help='Output directory')

parser.add_argument('--dispersal', type=float, default=1.0, help="Dispersal range")
parser.add_argument('--cover', default=0.32, type=float, help="Landscape cover")
parser.add_argument('--fragments', default=256, type=int, help="Landscape cover")

parser.add_argument("--defaults", default=None, help="Default community parameters")

# Misc parameters
parser.add_argument('--verbosity', default=1, help="Verbosity level of preparation script")

parser.add_argument('--dt', default=1.0)

args = parser.parse_args()

SIM_PATH = "./simulator/fragment-sim"
INPUT_DIR = args.directory
OUT_DIR = "{}/out/".format(INPUT_DIR)
TMP_DIR = "{}/tmp/".format(INPUT_DIR)
INIT_DIR = "{}/init".format(INPUT_DIR)
BATCH_FILE = "{}/batch.sh".format(INPUT_DIR)
INIT_COMMUNITY_LOCAL_FILE = "{}/community-normal.json".format(INPUT_DIR)
INIT_COMMUNITY_LIMIT_FILE = "{}/community-limit.json".format(INPUT_DIR)

DIRS_TO_CREATE = [INPUT_DIR, OUT_DIR, TMP_DIR, INIT_DIR]

# --- utilities ---

def log(s, level=1):
    if args.verbosity >= level:
        time_diff = time.time() - start_time
        print "[{} {}] {:.2f}\t{}".format(sys.argv[0], INPUT_DIR, time_diff, s)

def new_seed():
    return random.randrange(0, 2**32-1)

# --- Script ---

def init():
    log("Start time: {}".format(time.strftime("%H:%M:%S")))
    log("Scenario directory: '{}".format(INPUT_DIR))

    log(args)
    log("RNG seed '{}'".format(args.seed))
    random.seed(args.seed)

    for d in DIRS_TO_CREATE:
        log("Create directory '{}'".format(d))
        util.new_dir(d)


def init_community():
    log("Generate community data (regular) file '{}'".format(INIT_COMMUNITY_LOCAL_FILE))
    log("Generate community data (limiting) file '{}'".format(INIT_COMMUNITY_LIMIT_FILE))

    if args.defaults is not None:
        generate_community.defaults = json.load(open(args.defaults))

    community = generate_community.generate(
        species_count=1,
        dispersal_scale=args.dispersal,
        propagule_establishment=False,
        hops=1,
        gradient=False
    )

    json.dump(community, open(INIT_COMMUNITY_LOCAL_FILE, 'w'))

    # Prepare limiting version with scales -> U
    def set_limit_scale(d):
        if type(d) == dict:
            if "scale" in d:
                d["scale"] = args.U/2.0

            for k, v in d.items():
                set_limit_scale(v)

        if type(d) == list:
            for x in d:
                set_limit_scale(x)

    set_limit_scale(community)
    json.dump(community, open(INIT_COMMUNITY_LIMIT_FILE, 'w'))

def prepare_case(pars, typestr, community, index):
    sim_seed = new_seed()

    # If no fragmentation parameters passed, then just use intact landscape
    nofragments = pars is None

    fc = 1
    cc = 1.0

    if not nofragments:
        fc = pars["fragments"]
        cc = pars["cover"]

    basedir = "{}/{}-{}-{}/{}-{}".format(OUT_DIR,
                                      fc,
                                      cc,
                                      typestr,
                                      index,
                                      sim_seed)
    input_community_fname = "{}/community.json".format(basedir)
    output_snapshot_fname = "{}/out.snapshot".format(basedir)
    density_output_fname = "{}/density.txt".format(basedir)
    sim_script_fname = "{}/sim.sh".format(basedir)

    # Basic commands
    sim_str_base = "{sim} -U {u} -t {t} --dt {dt} -o {snapshot} -d {dens} -m {input_model} -s {seed} {input} > {initoutput}"

    # Create basedir
    util.new_dir(basedir)

    # Generate input community file
    input_json = copy.deepcopy(community)

    if not nofragments:
        fragments = fragmenter.fragment("lognorm", pars)
        input_json["fragments"] = [{"x" : f[0][0], "y" : f[0][1], "r" : f[1]} for f in fragments]
    json.dump(input_json, open(input_community_fname, 'w'))

    # Simulation cmd
    sim_str = sim_str_base.format(
            sim = SIM_PATH, 
            u = args.U, 
            dt = args.dt, 
            t = args.time, 
            snapshot = output_snapshot_fname, 
            input_model = input_community_fname, 
            seed = sim_seed, 
            dens = density_output_fname, 
            input = "",
            initoutput = "{}/stdout".format(basedir)
    )

    with open(sim_script_fname, 'w') as f:
        f.write(sim_str)
        f.write("\n")

    return basedir, sim_script_fname

def prepare_replicates():
    log("Initialise fragmentation simulation replicates")

    cases = []
    fp = {
        "variance" : 1.0,
        "cover" : args.cover,
        "fragments" : args.fragments
    }

    communities = {
        "limit" : json.load(open(INIT_COMMUNITY_LIMIT_FILE)),
        "local" : json.load(open(INIT_COMMUNITY_LOCAL_FILE))
    }

    for (typestr, community) in communities.items():
        for i in xrange(args.replicates):
            c = prepare_case(fp, typestr, community, i)
            cases.append(c)

        for i in xrange(args.replicates):
            c = prepare_case(None, typestr, community, i)
            cases.append(c)

    return cases

def prepare_batch(basedirs):
    commands = [cmd for (d, cmd) in basedirs]
    log("Writing batch commands to '{}'".format(BATCH_FILE))
    with open(BATCH_FILE, 'w') as f:
        for cmd in commands:
            setup.chmod_exec(cmd)
            f.write(cmd+"\n")

def main():
    init()
    init_community()
    basedirs = prepare_replicates()
    prepare_batch(basedirs)

if __name__ == '__main__':
    main()

