#!/usr/bin/env python
import sys
import argparse
import time
import json
import os.path

import util
import trapezoid

start_time = time.time()
parser = argparse.ArgumentParser()
parser.add_argument('suitedir', help='root directory')
parser.add_argument('--slosspanels', type=int, nargs="*", default=[], help='SLOSS panel indices, give list asx1 y1 x2 y2')
parser.add_argument('--slossindex', type=str, default="3A", help="Which scenario to highlight")
parser.add_argument('--outdir', help='output directory')
parser.add_argument('--verbosity', default=1, type=int, help='verbosity')

args = parser.parse_args()

INPUT_DIR = args.suitedir
while INPUT_DIR[-1] == '/':
    INPUT_DIR = INPUT_DIR[0:-1]

print(INPUT_DIR)

if args.outdir is None:
    OUT_DIR = "{}/analysis-output/".format(INPUT_DIR)
else:
    OUT_DIR = args.outdir

util.new_dir(OUT_DIR)

SLOSS_MATRIX_FIG = "{}/sloss-matrix.pdf".format(OUT_DIR)
SLOSS_HIGHLIGHTS_FIG = "{}/sloss-highlighted-panels.pdf".format(OUT_DIR)

lines = open('{}/casedirs.txt'.format(INPUT_DIR)).readlines()
casedirs = [l.strip() for l in lines]

def run_and_check(s):
    log("RUN: {}".format(s))
    if util.run(s) != 0:
        log("FAILURE: Error occurred when running '{}'".format(s))
        sys.exit(1)

def log(s, level=1):
    if args.verbosity >= level:
        time_diff = time.time() - start_time
        print "[{} {}] {:.2f}\t{}".format(sys.argv[0], INPUT_DIR, time_diff, s)

def make_sloss():
    log("Making SLOSS figures")
    sloss_files = ["{}/data-sloss-0.json".format(casedir) for casedir in casedirs]
    fragmentation_files = ["{}/fragmentation_parameters.json".format(casedir) for casedir in casedirs]
    community_files = ["{}/community.json".format(casedir) for casedir in casedirs]

    log("Input: {}".format(sloss_files))

    datas = []


    # Scenario to highlight
    scenario_id = args.slossindex
    assert len(args.slosspanels) % 2 == 0, "Need even number of elements to make (x,y) pairs {}".format(args.sloss_panels)
    xy_indices = [(args.slosspanels[i], args.slosspanels[i+1]) for i in range(0, len(args.slosspanels), 2)]

    for sid, (sfname, ffname, cfname) in enumerate(zip(sloss_files, fragmentation_files, community_files)):
        identifier = sfname.split('/')[-2][0:2]
        print "{} -> {}".format(sfname, identifier)
        datas.append({
            'data' : json.load(open(sfname)),
            'fragmentation' : json.load(open(ffname)),
            'total.species' : len(json.load(open(cfname))['model']['species']),
            'identifier' : identifier
        })

        print datas[-1]['total.species']

        cwidth = len(datas[-1]['fragmentation']['cover'])
        if identifier == scenario_id:
            datas[-1]['highlight'] = xy_indices
            indices = [cwidth*y+x for (x,y) in xy_indices]
            print "{} {}".format(indices, len(datas[-1]['data']))
            sub = [datas[-1]['data'][i] for i in indices]
            fig = trapezoid.plot_sloss_panel(sub, datas[-1]['total.species'])
            fig.set_size_inches(4.5*0.4,4.5)
            fig.savefig(SLOSS_HIGHLIGHTS_FIG)
            log("Saved SLOSS highlight fig to '{}'".format(SLOSS_HIGHLIGHTS_FIG))

    datas = sorted(datas, key = lambda x : (x['identifier'][1],x['identifier'][0]))

    m = 1.4285714285714286

    fig = trapezoid.plot_sloss_summary_matrix(datas, normalize=True, id_label=False)
    fig.set_size_inches(4.6*m,4.6)
    fig.savefig(SLOSS_MATRIX_FIG)
    log("Saved SLOSS matrix to '{}'".format(SLOSS_MATRIX_FIG))

def make_sfar():
    log("Generating SFAR plots")
    run_and_check("Rscript plot-sfar.r {} {}".format(OUT_DIR, " ".join(casedirs)))

def make_local_scale():
    log("Running local scale analysis")
    a = "Rscript analyse-focal.r {} {}".format(OUT_DIR, " ".join(casedirs))
    b = "Rscript plot-focal.r {} {}".format(OUT_DIR, " ".join(casedirs))
    run_and_check(a)
    run_and_check(b)

def make_density():
    log("Average density plots")
    a = "Rscript plot-average-density.r {} {}".format(OUT_DIR, " ".join(casedirs))
    run_and_check(a)

if __name__ == '__main__':
    make_sloss()
    make_sfar()
    make_local_scale()
    make_density()

