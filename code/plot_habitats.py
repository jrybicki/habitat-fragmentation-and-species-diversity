#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import json
import argparse
import matplotlib as mpl
import matplotlib.pyplot as pp
import collections
import math
import generate_input
from matplotlib.patches import Rectangle, Circle

use_gradient = False
extension = "pdf"

def draw_habitat(ax, centers, radius, integral):
    for p in centers:
        e, x, y = p
        overflows = []
        # Check if patch overflows
        if x-radius < 0:
            overflows.append( (U+x, y) )
        if y-radius < 0:
            overflows.append( (x, U+y) )
        if x+radius > U:
            overflows.append( (x-U, y) )
        if y+radius > U:
            overflows.append( (x, y-U) )

        if x+radius > U and y+radius > U:
            overflows.append( (x-U, y-U) )

        if x-radius < 0 and y-radius < 0:
            overflows.append( (x+U, y+U) )

        if x+radius > U and y-radius < 0:
            overflows.append( (x-U, y+U) )

        if x-radius < 0 and y+radius > U:
            overflows.append( (x+U, y-U) )

        overflows.append((x,y))
        for p2 in overflows:
            ax.add_patch(Circle(p2, radius, facecolor=colors[e], alpha=integral/P,zorder=1))

U = 100.0
colors = ['black', 'green', 'blue', 'orange', 'red', 'yellow']

P = 4.0

def density(cover, radius):
    return cover / (math.pi*radius**2)

d = 0.1 / 4.0
cases = [
 (d, 2, 1.5, 1, "a"),
 (d, 2, 0.75, 1, "b"),
 (d, 2, 1.5, int(P), "c"),
 (d, 2, 0.75, int(P), "d"),
]

if __name__ == '__main__':
    if len(sys.argv) >= 5:
        basename = sys.argv[1]
        seed = int(sys.argv[2])
        use_gradient = int(sys.argv[3])
        extension = sys.argv[4]
        generate_input.np.random.seed(seed)
    else:
        print("Usage: {} [basename] [seed] [gradient] [file extension]".format(sys.argv[0]))
        sys.exit(0)

    fig,axes = pp.subplots(2, 2, figsize=(10,10))
    figs = []


    axes = axes.flatten()
    for a in axes:
        a.set_ylim(0, U)
        a.set_xlim(0, U)

    panel = 1
    for a,c in zip(axes, cases):
        a.set_title("({}) Radius: {}, Density: {}".format(c[4],c[1],c[0]))
        ps = []
        for i in range(1,c[3]+1):
            if not use_gradient:
                ps.extend(generate_input.random_points(U, c[0], i))
            else:
                func = generate_input.magic_gradient(i)
                ps.extend(generate_input.random_points_gradient(U, c[0], i, func))
        print "Density: {}, number of points: {}".format(c[0], len(ps))
        draw_habitat(a,ps,c[1],c[2])

        # Draw a separate panel
        f,ax = pp.subplots(1,1, figsize=(5,5))
        ax.set_ylim(0,U)
        ax.set_xlim(0,U)
        draw_habitat(ax, ps, c[1], c[2])
        f.savefig("{}-panel-{}.{}".format(basename, panel, extension))
        panel += 1
        

    fig.savefig("{}.{}".format(basename, extension))
