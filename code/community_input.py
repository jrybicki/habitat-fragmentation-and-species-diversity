from collections import defaultdict

def make_input(community):
    mapping = defaultdict(list)
    entity_count = 0

    for s in community["species"]:
        mapping["satiated"].append(entity_count)
        entity_count += 1
        mapping["hungry"].append(entity_count)
        entity_count += 1

    for p in community["patches"]:
        mapping["patches"].append(entity_count)
        entity_count += 1

    for p in community["resources"]:
        mapping["resources"].append(entity_count)
        entity_count += 1

    out = {
        "entities" : mapping,
        "model" : community
    }

    return out
