import util
import sys
import os


basedir = sys.argv[1]
stylefile = "{}/style.json".format(basedir)
for f in util.find_recursive(basedir, 'out.snapshot'):
    pngfname = f.replace('out.snapshot', 'snapshot.png')
    print "Processing '{}' into '{}'".format(f,pngfname)
    tmpdir = f.replace('out.snapshot', 'tmp')
    cmd = "python animate.py --input {} --style {} --snapshot -1 --output {} --tmpdir {} --nolabel 1 --noaxis 1".format(f, stylefile, pngfname, tmpdir)
    util.run(cmd)
