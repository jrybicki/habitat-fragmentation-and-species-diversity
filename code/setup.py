#!/usr/bin/env python
import sys
import util
import random
import argparse
import time
import os
import stat

def chmod_exec(fname):
    st = os.stat(fname)
    os.chmod(fname, st.st_mode | stat.S_IEXEC)

def run(argv):
    print argv
    parser = argparse.ArgumentParser()
    parser.add_argument('rootdir', help='Output directory')
    parser.add_argument('--maxjobs', default=100, type=int)
    parser.add_argument('--cputime', default="24:00:00")
    parser.add_argument('--cpumem', default="1000")
    parser.add_argument('--partition', default="serial")
    args, unknown = parser.parse_known_args([sys.argv[0]].extend(argv))

    PREPARE_SCRIPT = "./prepare.py --directory {} {}".format(args.rootdir," ".join(unknown))
    ALL_BATCH = "{}/batch/all.sh".format(args.rootdir)
    COLLECT_BATCH = "{}/batch/collect.sh".format(args.rootdir)

    SEQUENTIAL_BATCH = "{}/sequential.sh".format(args.rootdir)
    PARALLEL_BATCH = "{}/parallel.sh".format(args.rootdir)
    SLURM_BATCH = "{}/slurm.sh".format(args.rootdir)


    sequential_script = """#!/bin/bash
{}
{}
{}
echo "DONE"
    """

    parallel_script = """#!/bin/bash
{}
parallel :::: {}
parallel :::: {}
echo "DONE"
    """

    slurm_script = """#!/bin/bash
srun --time={cputime} --partition={partition} {prepare_script}
./slurmify.py {simulate_script} {simulate_sbatch} --cputime={cputime} --partition={partition} --cpumem={cpumem} --maxjob={maxjobs} 
./slurmify.py {collect_script} {collect_sbatch} --cputime={cputime} --partition={partition} --cpumem={cpumem} --maxjob={maxjobs} 
echo "Submitting '{simulate_sbatch}'"
first=$(sbatch --parsable {simulate_sbatch})
echo "Submitting '{collect_sbatch}' depends on $first"
second=$(sbatch --dependency=afterany:$first {collect_sbatch})
squeue -u $USER
    """

    def log(s, level=1):
        print "[{} {}]\t{}".format("SETUP", args.rootdir, s)

    def parallel_setup():
        log("GNU Parallel batch: '{}'".format(PARALLEL_BATCH))
        with open(PARALLEL_BATCH, 'w') as f:
            f.write(parallel_script.format(PREPARE_SCRIPT, ALL_BATCH, COLLECT_BATCH))
        chmod_exec(PARALLEL_BATCH)

    def sequential_setup():
        log("Sequential batch: '{}'".format(SEQUENTIAL_BATCH))
        with open(SEQUENTIAL_BATCH, 'w') as f:
            f.write(sequential_script.format(PREPARE_SCRIPT, ALL_BATCH, COLLECT_BATCH))
        chmod_exec(SEQUENTIAL_BATCH)

    def slurm_setup():
        log("SLURM batch: '{}'".format(SLURM_BATCH))
        with open(SLURM_BATCH, 'w') as f:
            f.write(slurm_script.format(
                cputime=args.cputime,
                cpumem=args.cpumem,
                partition=args.partition,
                maxjobs=args.maxjobs,
                simulate_script=ALL_BATCH,
                collect_script=COLLECT_BATCH,
                prepare_script=PREPARE_SCRIPT,
                simulate_sbatch="{}/batch/all.sbatch".format(args.rootdir),
                collect_sbatch="{}/batch/collect.sbatch".format(args.rootdir),
            ))
        chmod_exec(SLURM_BATCH)

    log(args.rootdir)
    util.new_dir(args.rootdir)
    sequential_setup()
    parallel_setup()
    slurm_setup()

    return SEQUENTIAL_BATCH, PARALLEL_BATCH, SLURM_BATCH

if __name__ == '__main__':
    run(sys.argv)
