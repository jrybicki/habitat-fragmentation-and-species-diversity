library(dplyr)
library(data.table)
source("common.r")

# Input arguments
args <- commandArgs(trailingOnly = TRUE)
outdir <- args[1]
casedirs <- args[-1]

# Input files
files <- paste_filename_to_subdirs("focal-samples.csv.gz", casedirs)

gof <- function(m) {
    1 - m$deviance/m$null.deviance
}

fit <- function(df) {
    a <-  df %>% group_by(sample.radius, local.radius) %>% 
          do(data.frame(deviance = gof(glm(species ~ log(local.habitat), data=., family=poisson(link=log))))) %>%
          mutate(fitted.model="LL")

    b <-  df %>% group_by(sample.radius, local.radius) %>% 
          do(data.frame(deviance = gof(glm(species ~ log(fragment.area) + log(local.habitat), data=., family=poisson(link=log)))))%>%
          mutate(fitted.model="F+LL") 

    c <-  df %>% group_by(sample.radius, local.radius) %>% 
          do(data.frame(deviance = gof(glm(species ~ log(fragment.area), data=., family=poisson(link=log)))))%>%
          mutate(fitted.model="F") 

    d <-  df %>% group_by(sample.radius, local.radius) %>% 
          do(data.frame(deviance = gof(glm(species ~ log(fragment.area) + log(local.habitat) + log(landscape.cover) + log(landscape.fragments), data=., family=poisson(link=log)))))%>%
          mutate(fitted.model="F+LL+G") 

    e <-  df %>% group_by(sample.radius, local.radius) %>% 
          do(data.frame(deviance = gof(glm(species ~ log(fragment.area) + log(local.habitat) + log(landscape.cover) * log(landscape.fragments), data=., family=poisson(link=log)))))%>%
          mutate(fitted.model="F+LL+I") 

    f <-  df %>% group_by(sample.radius, local.radius) %>% 
          do(data.frame(deviance = gof(glm(species ~  log(landscape.cover) * log(landscape.fragments), data=., family=poisson(link=log)))))%>%
          mutate(fitted.model="I") 

    rbind(a,b,c,d,e,f)
}

analyse <- function(data) {
    af <- fit(data)

    # Get optimal radii; only care about LL
    opts <- af %>% 
            filter(fitted.model == "LL") %>%
            group_by(sample.radius) %>% 
            filter(deviance == max(deviance)) %>%
            rename(opt.ll.radius = local.radius)
            

    # 
    d <- af %>% ungroup() %>% 
                group_by(sample.radius) %>% 
                full_join(opts %>% select(-c(deviance,fitted.model)))

    # As SAR data, we only care about the amount of habitat  the optimal landscape radius
    sar_data <- data %>% 
                group_by(sample.radius) %>% 
                full_join(opts) %>% 
                filter(local.radius == opt.ll.radius) %>%
                select(-c(deviance,fitted.model))

    list(sar_data, d)
}

all_opts <- data.frame()
all_sar <- data.frame()

for (i in 1:9) {
    # Parse
    fname <- files[[i]]
    data <- read.csv(fname, header=TRUE)

    mode <- get_mode_and_dispersal(fname)

    # Compute areas
    data$fragment.area <- pi*data$fragment.radius^2
    data$local.habitat <- pi*data$local.radius^2 * data$local.cover


    # Prepare
    res <- analyse(data)
    res_sar <- res[[1]]
    res_opts <- res[[2]]
    
    res_sar$delta_val <- mode$delta_val
    res_sar$mode_val <- mode$mode_val
    res_opts$delta_val <- mode$delta_val
    res_opts$mode_val <- mode$mode_val
    

    if (i == 1) {
        all_sar <- res_sar
        all_opts <- res_opts
    } else{
        all_sar <- rbind(all_sar, res_sar)
        all_opts <- rbind(all_opts, res_opts)
    }
}



opts_fname = paste(outdir, "focal-opts.csv", sep="/")
sar_fname = paste(outdir, "focal-sar.csv", sep="/")
write.csv(all_opts, opts_fname, row.names=FALSE)
write.csv(all_sar, sar_fname, row.names=FALSE)
