library(ggplot2)
library(dplyr)

source("common.r")

# Input arguments
args <- commandArgs(trailingOnly = TRUE)
sim_fname <- args[1]
ode_fname <- args[2]
U <- as.double(args[3])
T <- as.double(args[4])
pdf_fname <- args[5]


data <- read.csv(sim_fname) # "meanfield/data-15x15-200.csv")
ode <- read.csv(ode_fname) # "meanfield/ode-density.csv")

ode <- ode %>%
       select(c("S0", "time", "landscape.cover")) %>%
       rename(meanfield = S0) %>%
    mutate(time = as.integer(time))



d <- data %>%
    mutate(time = as.integer(time)) %>%
     full_join(ode) %>%
     filter(time < T) %>%
     group_by(landscape.cover,
              landscape.fragments,
              simulation.type,
              replicate.id)

sd <- d %>%
    group_by(time, landscape.cover, landscape.fragments, simulation.type) %>%
    summarise(avg = mean(S0))

pdf(pdf_fname, width=4, height=4)

ggplot(d %>% filter(replicate.id > 5 && replicate.id <= 10),
       aes(x=time, y=S0/U^2, color=as.factor(replicate.id))) +
    ylab("Density per unit area") +
    facet_grid(landscape.fragments ~simulation.type) +
    geom_line(aes(x=time, y=meanfield), color="grey", size=2.5) +
    geom_line(data=sd, aes(x=time, y=avg/U^2), color="black", size=1) +
    geom_line(alpha=0.8) +
    scale_colour_discrete(name="Replicate") +
    theme_classic() +
    get_theme() +
    theme(legend.position='none')

dev.off()

