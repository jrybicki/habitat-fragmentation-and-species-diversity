# Style

STYLES = {
    "colors" : ["black", "black", "gray", "red", "purple", "green", "blue"],
    "sizes" : [0, 10, 10, 10, 20, 20, 10, 10],
    "alphas" : [0, 0.1, 1, 1, 1, 1, 1, 1],
    "markers" : ['.', '.', 'o', 'o', '.', 'x', 'o'],
    "labels" : ["?", "Tissue", "Leuko", "Bact.", "Tox.", "Dec.", "Act."]
}
