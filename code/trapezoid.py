import json
import sys
import numpy
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 8})

def approximate_integral(rs, bs):
    def slope(p,q):
        dx = q[0]-p[0]
        dy = q[1]-p[1]
        return float(dy)/float(dx)

    def make_sweep_list(rs, bs):
        # Assume Rs has the smallest x coordinate
        rs = sorted(rs)
        bs = sorted(bs)

        assert rs[0] <= bs[0]

        new_rs = []
        new_bs = []

        xs = sorted(set([p[0] for p in rs+bs]))
        heights = []
        b_slope = None
        r_slope = slope(rs[0], rs[1])

        for x in xs:
            if x == bs[0][0] and x == rs[0][0]:
                new_bs.append( bs.pop(0) )
                new_rs.append( rs.pop(0) )
                continue
            
            if x == bs[0][0]:
                # Add virtual R point
                y = (x-rs[0][0])*r_slope + rs[0][1]
                new_rs.append((x,y))

                b_slope = slope(bs[0], bs[1])
                new_bs.append( bs.pop(0) )
            elif rs[0][0] == x:
                # Add virtual B point
                if b_slope is not None:
                    y = (x-bs[0][0])*b_slope + bs[0][1]
                    new_bs.append((x,y))
                r_slope = slope(rs[0], rs[1])
                new_rs.append( rs.pop(0) )
            else:
                raise Exception("am confus")

        while new_rs[0][0] < new_bs[0][0]:
            new_rs.pop(0)
        assert len(new_rs) == len(new_bs)

        return new_rs, new_bs

    rs, bs = make_sweep_list(rs, bs)
    points = [ (p[0], p[1]-q[1]) for (p,q) in zip(rs,bs)]
    areas = []
    A = 0.0
    for p,q in zip(points, points[1:]):
        h = q[0]-p[0]
        a = h*(p[1]+q[1])/2.0
        A += a
        areas.append(a)

    return A, rs, bs

def plot_sloss_summary(data):
    A = []
    Ns = set()
    Cs = set()
    Xs = {}
    for d in data:
        rs = d['sl']
        bs = d['ls']
        bs[-1] = rs[-1] # hack; they should be the same but floats cause issues
        area, new_rs, new_bs = approximate_integral(rs, bs)
        d['area'] = area 
        n,c = d['info']
        Ns.add(n)
        Cs.add(c)
        A.append(area)
        Xs[(n,c)] = area

    A = list([Xs[v] for v in sorted(Xs.keys())])
    A = numpy.array(A)
    A = A.reshape(len(Ns),len(Cs))
    vv = max(map(abs, [A.max(), A.min()]))
    fig, ax = plt.subplots()
    heatmap = ax.pcolor(A, cmap=plt.cm.bwr_r, vmin=-vv, vmax=vv)

    cbar = fig.colorbar(heatmap)
    cbar.ax.set_yticklabels(['negative', 'no effect', 'positive'])
    ax.set_yticklabels(sorted(Ns), minor=False)
    ax.set_xticklabels(sorted(Cs), minor=False)
    ax.set_yticks(numpy.arange(A.shape[0])+0.5, minor=False)
    ax.set_xticks(numpy.arange(A.shape[1])+0.5, minor=False)
    fig.gca().invert_yaxis()
    return fig

def plot_sloss_summary_matrix(datas, normalize=False, labels=False, id_label=False):
    fig, axes = plt.subplots(3,3, sharex=True, sharey=True)
    vv = 8.0

    for ax, data in zip(axes.flatten(), datas):
        A = []
        Ns = set()
        Cs = set()

        for d in data['data']:
            rs = d['sl']
            bs = d['ls']
            bs[-1] = rs[-1] # hack; they should be the same but floats cause issues
            area, rs, bs = approximate_integral(rs, bs)

            Ns.add(d['info'][0])
            Cs.add(d['info'][1])

            if normalize:
                max_a, max_s = bs[-1]
                area /= max_a #  d['info'][1]

            d['area'] = area 
            A.append(area)
            print "N={} C={} I={}".format(d['info'][0], d['info'][1], area)

        print Ns
        print Cs
        shape = (len(Ns), len(Cs))

        assert shape[0] == len(data['fragmentation']['fragments']) - 1, "{} != {}".format(Ns, data['fragmentation']['fragments'])
        assert shape[1] == len(data['fragmentation']['cover']), "{} != {}".format(Cs, data['fragmentation']['cover'])

        A = numpy.array(A)
        A = A.reshape(*shape)

        vv = max(vv, A.max())
        print A.max(), A.min()

        if id_label:
            ax.text(0, 0.5, data['identifier'], va='center', ha='center')
        ax.set_yticklabels(sorted(Ns), minor=False)
        ax.set_xticklabels(["{0:g}%".format(float(s)*100) for s in sorted(Cs)], minor=False, rotation=90)
        ax.set_yticks(numpy.arange(A.shape[0])+0.5, minor=False)
        ax.set_xticks(numpy.arange(A.shape[1])+0.5, minor=False)
        heatmap = ax.pcolor(A, cmap=plt.cm.bwr_r, vmin=-vv, vmax=vv)

        if labels:
            for x in range(shape[0]):
                for y in range(shape[1]):
                    text = "+" if A[x,y] > 0 else "--"
                    #"{:.1f}".format(A[x,y])
                    ax.text(y+0.5, x+0.5, text, ha="center", va="center", fontsize="4")

        if 'highlight' in data:
            for (x,y) in data['highlight']:
                rect = matplotlib.patches.Rectangle((x,y),1.0,1.0,linewidth=2,edgecolor='black',facecolor='none')
                ax.add_patch(rect)
        plt.xticks(rotation=90)

    # FIXME: Relative ordering
    coltitles = ["Passive-Hostile", "Passive-Habitable", "Active-Habitable"]
    rowtitles = ["Short ($\delta=1$)", "Medium ($\delta=3$)", "Long ($\delta=10$)"]
    for ax, t in zip(axes[0], coltitles):
        ax.set_title(t)

#    pads = [-3, -3, -8]
    for ax, t in zip(axes[:,-1], rowtitles):
        ax.yaxis.set_label_position("right")
        ax.set_ylabel(t, rotation=270, size='large',labelpad=12.0)

    fig.subplots_adjust(hspace=0.1,wspace=0.1, right=0.8)

    cbar_ax = fig.add_axes([0.85, 0.12, 0.02, 0.75])
    cb = fig.colorbar(heatmap, cax=cbar_ax, ticks = [-vv, 0, vv])
    cb.ax.set_yticklabels(['negative', 'no effect', 'positive']) 

    for ax in axes.flatten():
        ax.invert_yaxis()


    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axis
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    plt.ylabel("#fragments", size='large', labelpad=10)
    plt.xlabel("habitat cover", size='large',labelpad=25)

    fig.subplots_adjust(bottom=0.2)

    return fig

def plot_sloss_panel_to_ax(fig, d, ax, max_species=64.0, max_cover=0.16):
    orig_rs = d['sl']
    orig_bs = d['ls']
    orig_bs[-1] = orig_rs[-1] # hack; they should be the same but floats cause issues
    area, rs, bs = approximate_integral(orig_rs, orig_bs)
    color = 'red' if area < 0 else 'blue'
    xs = [p[0] for p in rs+bs[::-1]]
    ys = [p[1] for p in rs+bs[::-1]]

    print "sloss panel max species", max_species
    ax.fill(xs,ys,color=color,alpha=0.33)
    ax.set_ylim(0,max_species)
    ax.set_xlim(0,max_cover)
    

    spacing = int(max_species) / 4
    ytix = list(xrange(0, int(max_species)+1, spacing))
    b = max_cover/4.0
    xtix = [i*b for i in xrange(0,5)]
    xlabels = ["{}%".format(int(100*x)) for x in xtix]

    print "max x {} max y {}".format(max(xs),max(ys))
    ax.set_yticklabels(ytix, minor=False)
    ax.set_yticks(ytix)

    ax.set_xticklabels(xlabels)
    ax.set_xticks(xtix)

#    fig.legend(loc=4)

    ax.set_aspect(max_cover/max_species)
#    ax.set_aspect(maxx/32.0)
    ax.plot(*zip(*orig_rs), label="SL", color='black', linewidth=2.75)
    ax.plot(*zip(*orig_bs), label="LS", color='black', linewidth=2.75)
    ax.plot(*zip(*orig_rs), label="SL", color='#ff7f0e', linewidth=1.25)
    ax.plot(*zip(*orig_bs), label="LS", color='#1f77b4', linewidth=1.255)

def plot_sloss_panel(data, total_species):
#    print data
    ns = int(len(data)**0.5)
    fig, axes = plt.subplots(3,1,sharex=True,sharey=True)

    for ax, d in zip(axes,data):
        print "d", d.keys()
        plot_sloss_panel_to_ax(fig, d, ax, max_species=total_species)
        ax.set_aspect('auto')
        max_cover = d['ls'][-1][0]
        print "MAX COVER", max_cover
        ax.set_xlim(0.0,max_cover)
        ax.set_ylim(0,total_species)
        xtix = [0, max_cover/2.0, max_cover] 
        print xtix
        xlabels = ["{}%".format(int(100*x)) for x in xtix]
        ax.set_xticklabels(xlabels)
        ax.set_xticks(xtix)

    fig.subplots_adjust(hspace=0.1,wspace=-0.0, right=0.8,left=0.25)

    fig.text(0.0, 0.5, 'Number of species', va='center', rotation='vertical',size='large')
    axes[-1].set_xlabel('Cumulative cover', size='large')
    return fig

