import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import csv
import json
import argparse
import copy

def model(X,t,parameters):
    beta = parameters["patch.turnover"]*parameters["patch.density"]
    gamma = parameters["patch.turnover"]
    kappa = parameters["resource.decay"]
    G = parameters["resource.rate"]
    U = parameters["consumption"][0]["rate"]
    eta = parameters["hunger"]
    mu = parameters["mortality"]
    alpha = parameters["immigration"]
    B = parameters["birth.rate"]

    print beta, gamma, kappa, G, U, eta, mu, alpha, B

    return [
        beta - gamma * X[0], # dP/dt
        G*X[0] - kappa*X[1] - U*X[3]*X[1], # dR/dt
        U*X[3]*X[1] - eta*X[2], # dS/dt
        alpha + (eta+B)*X[2] - mu*X[3] - U*X[3]*X[1] # dD/dt
    ]

def solve(parameters):
    # initial values
    X0 = [parameters["patch.density"],
        0.0,
        0.0,
        parameters["initial.hungry.density"]]

    Xs = odeint(model, X0, ts, args=(parameters,))
    return Xs

def get_rows(Xs):
    patches = Xs[:,0]
    resources = Xs[:,1]
    satiated = Xs[:,2]
    deprived = Xs[:,3]

    for t, row in zip(ts, Xs):
        yield {
            "time" : t,
            "simulation.type" : "meanfield.ode",
            "P0" : row[0],
            "R0" : row[1],
            "S0" : row[2],
            "H0" : row[3]
        }


def write_densities(parameters):
    fname = args.output # "ode-density.csv"
    cols = ["time", "R0", "P0", "S0", "H0", "simulation.type", "landscape.cover"]
    w = csv.DictWriter(open(fname, 'w'), fieldnames = cols)

    w.writeheader()
    for cover in args.cover:
        ps = copy.deepcopy(parameters)
        ps["resource.rate"] *= cover
        for row in get_rows(solve(ps)):
            row["landscape.cover"] = cover
            w.writerow(row)

def show_plot(parameters):
    """" Plot the meanfield model with equilibrium """
    beta = parameters["patch.turnover"]*parameters["patch.density"]
    gamma = parameters["patch.turnover"]
    kappa = parameters["resource.decay"]
    G = parameters["resource.rate"]
    U = parameters["consumption"][0]["rate"]
    eta = parameters["hunger"]
    mu = parameters["mortality"]
    alpha = parameters["immigration"]
    B = parameters["birth.rate"]


    # Equilibrium calculations
    a = beta*B*U*G
    b = gamma*eta
    c = kappa*mu
    d = alpha*U
    xi = np.sqrt((a + b*(c+d))**2 - 4*a*b*c)

    #print a,b,c,d,xi

    eqs = [
        beta / gamma,
        (a + b*(c+d) - xi)*1.0/(2.0*B*U*kappa*gamma),
        (a - b*(c+d) + xi)*1.0/(2.0*B*U*eta*gamma),
        (a + b*(d-c) + xi)*1.0/(2.0*U*eta*mu*gamma)
    ]


    Xs = solve(parameters)
    patches = Xs[:,0]
    resources = Xs[:,1]
    satiated = Xs[:,2]
    deprived = Xs[:,3]

    #print "Equilibrium points are", eqs

    colors = ["darkorange", "royalblue", "green", "violet"]

    # Plot equilibrium points
    tpad = 10
    for eq, label, col in zip(eqs, "PRSD", colors):
        plt.hlines( eq, -tpad, T+tpad,
                    linestyles="dashed",
                    label=label,
                    linewidth = 2,
                    alpha = 0.5,
                    color = col)

    # Dynamics
    lw = 3
    plt.plot(ts, patches, label="Patches", color=colors[0], linewidth=lw)
    plt.plot(ts, resources, label="Resources",  color=colors[1], linewidth=lw)
    plt.plot(ts, satiated, label="Satiated", color=colors[2], linewidth=lw)
    plt.plot(ts, deprived, label="Deprived", color=colors[3], linewidth=lw)

    plt.xlabel("Time")
    plt.ylabel("Population")
    plt.legend();

    plt.show()


# --- main ---

parser = argparse.ArgumentParser()
parser.add_argument('-T','--time',
                    type=float,
                    required=True,
                    help='Max time')
parser.add_argument("--parameters", required=True, help="Default community parameters")
parser.add_argument("--output", help="Output csv")
parser.add_argument('--cover', help="delimited list of cover parameter", type=float, nargs="*")
args = parser.parse_args()

parameters = json.load(open(args.parameters))
T = args.time
dt = 1.0
ts = np.linspace(0, T, T/dt)


if args.output is None:
    show_plot(parameters)
else:
    write_densities(parameters)
