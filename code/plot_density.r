require('ggplot2')
require('reshape2')

args = commandArgs(trailingOnly=TRUE)
area <- as.double(args[1])
filename <- args[2]
out <- args[3]

data <- read.table(filename, header=TRUE)
mf <- melt(subset(data, ,-c(events)),id="time")

pdf(out)
ggplot(mf, aes(x=time,y=value/area,colour=variable)) + geom_line() + theme_bw()+ theme(legend.position="bottom") + xlab("time") + ylab("density")

sdata2 <- subset(data, select=grep("^S|time", names(data), value=TRUE))
smf2 <- melt(sdata2, id="time")
ggplot(smf2, aes(x=time,y=value/area,colour=variable)) + geom_line() + theme_bw()+ theme(legend.position="bottom") + xlab("time") + ylab("density")

sdata2 <- subset(data, select=grep("^H|time", names(data), value=TRUE))
smf2 <- melt(sdata2, id="time")
ggplot(smf2, aes(x=time,y=value/area,colour=variable)) + geom_line() + theme_bw()+ theme(legend.position="bottom") + xlab("time") + ylab("density")

sdata2 <- subset(data, select=grep("^R|time", names(data), value=TRUE))
smf2 <- melt(sdata2, id="time")
ggplot(smf2, aes(x=time,y=value/area,colour=variable)) + geom_line() + theme_bw()+ theme(legend.position="bottom") + xlab("time") + ylab("density")

sdata2 <- subset(data, select=grep("^P|time", names(data), value=TRUE))
smf2 <- melt(sdata2, id="time")
ggplot(smf2, aes(x=time,y=value/area,colour=variable)) + geom_line() + theme_bw()+ theme(legend.position="bottom") + xlab("time") + ylab("density")


dev.off()
