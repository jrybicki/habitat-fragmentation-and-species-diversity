library(dplyr)
library(ggplot2)
library(grid)
library(gridExtra)
library(scales)

source("common.r")

# Input arguments
args <- commandArgs(trailingOnly = TRUE)
outdir <- args[1]
casedirs <- args[-1]

# Input files
sfar_files <- paste_filename_to_subdirs("data-sfar.csv", casedirs)
    
# Output files
cover_fname <- paste(outdir, "cover-summary.pdf", sep="/")
sfar_fname <- paste(outdir, "sfar-summary.pdf", sep="/")
hill1_fname <- paste(outdir, "sfar-summary-hill-1.pdf", sep="/")
hill2_fname <- paste(outdir, "sfar-summary-hill-2.pdf", sep="/")
individuals_fname <- paste(outdir, "sfar-summary-individuals.pdf", sep="/")

# --- Read SFAR data file ---

formatter <- function(xs){ 
    xs = xs*100
    mapply(function(x) 
    if (x > 1) { 
        sprintf("%i%%", round(as.integer(x)))
    } else { 
        sprintf("%0.1f%%", x)
    }, xs)
}



plot_ar <- function(d, varname, ylabel, outpdf, partial=FALSE) {
    data <- d %>% select(x=cover, y=varname, delta_val, mode_val, fragments)

    max_y <- max(data$y)
    min_y <- max(1, data$y)

    x_breaks = c(0.00125, 0.005,0.02,0.0801,0.32)
    x_labels = c(".1%", ".5%", "2%", "8%", "32%")

    y_breaks = c(1, 2, 4, 8, 16, 32, 64, 128)
    print(unique(data$x))

    if (partial) {
        d <- data %>% filter(fragments %in% c(1,16,64,1024))
    } else {
        d <- data
    }

    p <- ggplot(d %>% filter(y > 0.25), aes(x=x, y=y, color=as.factor(fragments))) +
         geom_point(size=0.5, alpha=0.75) +
         geom_line(alpha=0.75) + 
         facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
         theme_classic() +
         xlab("habitat cover") + 
         ylab(ylabel) +
         scale_color_manual(values=sfar_colors, name="#fragments") + 
         get_theme() + 
         coord_fixed() + 
         ylim(min_y, max_y) +
         scale_y_continuous(trans=log2_trans(),
                            breaks = trans_breaks("log2", function(x) 2^x),
                            labels = trans_format("log2", function(x) 2^x)) +
         scale_x_continuous(trans=log2_trans(), 
                            breaks=x_breaks, 
                            labels=x_labels) 

    pdf(outpdf, width=4.2, height=4.2)
    print(p)
    dev.off()
}

plot_cover <- function(d, varname, ylabel, outpdf) {
    data <- d %>% select(x=fragments, y=varname, delta_val, mode_val, cover)

    max_y <- max(data$y)
    max_y <- 8*ceiling(max_y/8.0)

    min_y <- max(1, min(data$y))
    y_breaks = seq(0, max_y, max_y/4)

    data <- data %>% filter(cover <= 0.32 & cover > 0.002)
    p <- ggplot(data, aes(x=x, y=y, color=as.factor(cover))) +
         geom_point(size=0.75) +
         geom_line() + 
         facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
         theme_classic() +
         xlab("#fragments") + 
         ylab(ylabel) +
        get_theme() +
        ylim(0, max_y) + 
        scale_colour_discrete(name="Habitat cover", labels = function(x) { paste(as.double(x)*100,"%",sep="")} ) +
        scale_y_continuous(breaks=y_breaks) + 
        scale_x_continuous(trans=log2_trans(),
                            breaks = trans_breaks("log2", function(x) 2^x),
                            labels = trans_format("log2", function(x) 2^x)) 

#                            labels = trans_format("log2", function(x) 2^x)) 

    pdf(outpdf, width=4.2, height=4.2)
    print(p)
    dev.off()
}

# --- main --- 

data <- read_suite_data(sfar_files)


# Species vs. area
plot_cover(data, "species", "#species", cover_fname)

# Species vs. area
plot_ar(data, "species", "#species", sfar_fname, partial=TRUE)

# Hill number of order 1 vs. area
plot_ar(data %>% filter(hill.1 != Inf), "hill.1", "H", hill1_fname)

# Hill number of order 2 vs. area
plot_ar(data %>% filter(hill.2 != Inf), "hill.2", "H", hill2_fname)

# Number of individuals vs. area
plot_ar(data, "individuals", "#individuals", individuals_fname)

