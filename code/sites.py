import sloss
import sys
import json
import util
import math
import glob

rootdir = sys.argv[1]
data = sloss.read_data(rootdir)
order = 0 # order of the hill number

print "fragments,landscape.cover,site.id,species,fragment.area,replicate,sample.radius"
for (fragments, cover), replicate_paths in data.items():
    for basedir in replicate_paths:

        community = json.load(util.open_file("{}/community.json".format(basedir)))
        entity_map = sloss.get_entity_map(community)
        for indata_fname in glob.glob("{}/output*json.gz".format(basedir)):
            indata = json.load(util.open_file(indata_fname))
            sites = indata["data"][-1]["sample.counts"]
            satiated_at_sites = [sloss.get_type_count("satiated", counts, entity_map) for counts in sites]
            species_count_at_sites = [sloss.hill(ss, order) for ss in satiated_at_sites]

            if not "sample.sites" in indata:
                continue

            coord_to_area_mapping = { (f["x"],f["y"]) : 2*math.pi*f["r"]**2 for f in indata["scaled.fragments"] }
            site_areas = [ coord_to_area_mapping[(s["x"], s["y"])] for s in indata["sample.sites"] ]

            for (site, (count, area)) in enumerate(zip(species_count_at_sites, site_areas)):
                print "{},{},{},{},{},{},{}".format(fragments, cover, site, count, area, basedir.split("/")[-1],indata["sample.window.radius"])

