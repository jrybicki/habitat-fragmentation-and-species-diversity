#!/bin/bash

INPUT_DIR=$1
DISPERSAL=$2 # dispersal scale
ESTABLISHMENT=$3 # no establishment restriction
INIT_SEED=$4 # 15177458227
SIM_SEED=$5 # 20638553997
HOPS=$6 # 1 # single hop during the life time of a propagule (birth)
GRADIENT=$7

BASE_COMMUNITY="$INPUT_DIR/community.json"
BATCH="$INPUT_DIR/batch.sh"
OUT_DIR="$INPUT_DIR/out/"
TMP_DIR="$INPUT_DIR/tmp/"
PARAM_FILE="illustrations/parameters-illustration.json"

U=100.0
INIT_T=100.0
SIM_T=100.0
REPLICATES=1
SPECIES=8
DT=1.0

mkdir -p $INPUT_DIR
mkdir -p $OUT_DIR
#python generate_community.py $SPECIES $DISPERSAL $ESTABLISHMENT $HOPS $GRADIENT > $BASE_COMMUNITY
#./init.sh $INPUT_DIR $INIT_SEED $INIT_T $U 
#python fragment-replicates.py $BASE_COMMUNITY $REPLICATES $OUT_DIR $SIM_SEED $INPUT_DIR/init/initial.state.txt $U $SIM_T $DT $PARAM_FILE > $BATCH
#parallel :::: $BATCH
python anim_style.py $INPUT_DIR/community.json > $INPUT_DIR/style.json
python make_illustration_data.py $INPUT_DIR
python make_grid_plot.py $INPUT_DIR $INPUT_DIR/examples.pdf

for fn in $INPUT_DIR/{init,out/*/*}/out.snapshot ; do
    echo $fn
    dn=$(dirname $fn)
    id=$(echo -n $fn | cut -d'/' -f5) 
    id2=$(echo -n $fn | cut -d'/' -f6) 
    if [[ $fn = *"init"* ]]; then
        id="intact"
    fi
    python animate.py --input $fn --out "$INPUT_DIR/animation-$id-$id2.gif" --tmpdir "$dn/gif/" --style $INPUT_DIR/style.json --size 3 3 --dpi 300 --noaxis 1
done
