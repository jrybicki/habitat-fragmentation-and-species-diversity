dir=$1
seed=$2
T=$3
U=$4
mkdir $dir/init
./simulator/fragment-sim -U $U -t $T -o $dir/init/out.snapshot -m $dir/community.json -s $seed -d $dir/init/density.txt
./get-final-state.sh $dir/init/out.snapshot > $dir/init/initial.state.txt
