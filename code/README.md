# Source code for the metacommunity model in "Habitat fragmentation and species diversity in competitive communities"

This package contains the source code for simulator and simulation experiments used in the following manuscript:

    Habitat fragmentation and species diversity in competitive species communities
    Joel Rybicki, Nerea Abrego, and Otso Ovaskainen
    Date: November 2019

Below are the instruction on how to run the simulations and replicate the analyses that appear in the manuscript and its supplementary material.

## Requirements and dependencies


### Required packages

For running the simulations, you need to have a C++ compiler and the following additional software and packages installed:

* [R 3.3](https://www.r-project.org/)
    * ggplot2
    * dplyr
    * gridExtra
* [Python 2.7](https://www.python.org/)
    * numpy (1.12.0)
    * matplotlib (2.0.0)
* [ImageMagick](https://www.imagemagick.org/)
* [Boost C++ libraries](http://www.boost.org/)


### Additional dependencies

The simulator also relies on the following libraries, which are included in `simulator/external/include`.

* [JSON for Modern C++](https://github.com/nlohmann/json)
* [CXXopts](https://github.com/jarro2783/cxxopts)
* [PCG random number generator for C++](https://github.com/imneme/pcg-cpp/)
* [Catch](https://github.com/philsquared/Catch.git)

The software has been tested on MacOS X 10.14 (Mojave) and on CentOS 6 (GNU/Linux). 


#### Note

The simulator is an extended version of the basic point process simulator that used in the the paper [Model of bacterial toxin-dependent pathogenesis explains infective dose](https://doi.org/10.1073/pnas.1721061115).

## Compilation

First, install all the required packages listed in the above section (Required packages). Second, in the root directory, run the following:

    make simulator

This compiles the point process simulator contained in [`./simulator`](./simulator). 

## Generating the snapshot illustrations and animations

Once you have successfully compiled the simulator, you can generate the snapshot figures and animations by running

    make snapshot-illustrations

Once the script completes, the resulting figures and animations are found in the `./illustrations` directory.

## Re-running the simulations

You can use the `make` command to re-run the simulation and analyses. You can type

    make help

to get a list of targets. 


Each target starts with a prefix. The prefix identifies the experiment suite. For example, the prefix `s128-100x100-basic-neutral` denotes the experiment with a neutral community of $S=128$ species in a $100 \times 100$ landscape using the default (`basic`) parameterisation, wheras `s128-100x100-basic-gradient` gives the variant of this experiment on landscapes with an environmental gradient in resource types.

Running the simulations and analysis for an experiment suite consists of the following steps:

1. setup (create batch files)
2. run simulations (create raw data)
3. run analysis (create plots).

As most of the experiments take a fairly long time to be completed, it is recommended that the experiments are run on a high-performance computing cluster.

### Simulation setup

The setup step is used to generate all the necessary batch files to be run on a your machine or using SLURM in a computing cluster. As an example, we will go walk through how to complete the small experiment suite `test-25x25-basic-neutral`. This experiment suite should be possible to run on any fairly recent computer in a reasonable amount of time.

First, to generate the batch files run:

    make test-25x25-basic-neutral

Once complete, there should be three batch files:

     ./results/test-25x25-basic-neutral/sequential.sh
     ./results/test-25x25-basic-neutral/parallel.sh
     ./results/test-25x25-basic-neutral/slurm.sh

You can run any of the three scripts by calling

    sh ./results/test-25x25-basic-neutral/SCRIPTNAMEHERE

The first one can be used to run all the experiments sequentially in a single CPU core. If you have [GNU parallel](https://www.gnu.org/software/parallel/) installed, you can run the simulations in parallel by calling the second script. However, in most cases, the simulations are very time consuming so it is recommended to use the last script in a computing cluster environment with [SLURM](https://slurm.schedmd.com/) installed.


### Analysis of results

Once all the simulations in a single experiment suite are complete, you can run the python and R scripts that analyse the results. This is done by calling

     make test-25x25-basic-neutral

Once complete, the results appear in the directory

     ./results/test-25x25-basic-neutral/analysis-output/

### Experiment suites

As discussed above, the `Makefile` contains targets for several experiment suites for various parameterisations of the model. You can use

    make help

to get the complete list of targets. 


### Comparison against the meanfield model

The simple comparison against the single-species meanfield model can be re-run as follows. First, to create the simulation data, run

    make meanfield-setup

and then run the resulting simulation batch. (Unfortunately, the simulations where interaction kernels have large length scales tend to be very slow to run with this model.)

Afterwards, to produce the meanfield comparison figure run

    make meanfield-analysis


To show a plot of the meanfield dynamics over time, run 

    make meanfield-plot
    
