"""
Generate input data points for community simulation according to given parameters.
"""

import numpy as np
import itertools as it

def random_points(U, density, entity):
    n = int(U**2 * density)
    xs = np.random.uniform(0, U, n)
    ys = np.random.uniform(0, U, n)
    for (x,y) in zip(xs,ys):
        yield (entity,x,y) 

def random_points_gradient(U, density, entity, dfunc):
    def rejection_sampling():
        while True:
            x,y = np.random.uniform(0,1,2)
            if dfunc(x,y) >= np.random.uniform(0,1):
                return (x*U,y*U)

    n = int(U**2 * density)
    for i in xrange(0,n):
        x,y = rejection_sampling()
        yield (entity, x, y)


""" MAGIC FUNCTIONS for gradients; see ppsimulator.c and check that these match """

def magic_gradient(t):
    func = None
    if t == 1:
        func = lambda x,y : (1.0+np.sin(2*np.pi*x))/2.0
    elif t == 2:
        func = lambda x,y : (1.0+np.sin(2*np.pi*x+np.pi))/2.0
    elif t == 3:
        func = lambda x,y : (1.0+np.sin(2*np.pi*y))/2.0
    elif t == 4:
        func = lambda x,y : (1.0+np.sin(2*np.pi*y+np.pi))/2.0
    else:
        raise Exception("Unknown gradient for patch {}".format(t))
    return func



def random_points_in_circle(R, n):
    rs = R*np.sqrt(np.random.uniform(0,1,n))
    ts = np.random.uniform(0, 2*np.pi, n)
    xs = rs * np.cos(ts)
    ys = rs * np.sin(ts)
    return xs, ys

