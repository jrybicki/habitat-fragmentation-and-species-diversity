f=$1
seed=$2
python summarise-focal.py $f/out/ > $f/focal.json
python sample-focal.py $seed < $f/focal.json > $f/focal-samples.csv
gzip -f $f/focal-samples.csv
