import sys
import json
import util
import sloss
import csv
import re
import collections
import glob
import os
import pickle

order = 0
datadir = sys.argv[1]

def get_data(datadir):
    scenario, case_id = datadir.split("/")[-2:]
    fragment_count, cover = scenario.split("-")

    community_data = json.load(open("{}/community.json".format(datadir)))
    output_fnames = glob.glob("{}/output*.json.gz".format(datadir))

    for of in output_fnames:
        count_data = json.load(util.open_file(of))
        local_fname = of.replace("output","local")
        focal_data = json.load(util.open_file(local_fname))

        if focal_data == None:
            return

        last_data = count_data["data"][-1]

        entity_mapping = sloss.get_entity_map(community_data)

        combined = {}

        for (index, (site, counts)) in enumerate(zip(count_data['sample.sites'], last_data["sample.counts"])):
            x = site['x']
            y = site['y']
            sdata = sloss.get_type_count("satiated", counts, entity_mapping)
            combined[(x,y)] = site
            combined[(x,y)]['satiated'] = sdata
            combined[(x,y)]['speciescount'] = sloss.hill(sdata, order)
            combined[(x,y)]['site.id'] = index

        for data in focal_data["data"]:
            x = data["x"]
            y = data["y"]
            scount = combined[(x,y)]['speciescount']
            siteid = combined[(x,y)]['site.id']

            for r in data["results"]:
                r["x"] = data["x"]
                r["y"] = data["y"]
                r["fragment.radius"] = data["patch.radius"]
                r["species"] = scount
                r["landscape.fragments"] = fragment_count
                r["landscape.cover"] = cover
                r["site.id"] = siteid
                r["case.id"] = case_id
                r["sample.radius"] = count_data["sample.window.radius"]
                yield r

def get_all(dirs):
    for d in dirs:
        for r in get_data(d):
            yield r

def get_iterator():
    rootdir = sys.argv[1]
    pattern = re.compile(".*/([0-9]+)-(0.[0-9]+)/")
    d = collections.defaultdict(list)
    dirs = []
    for rdir in glob.iglob(rootdir+'*/'):
        for subdir in glob.iglob(rdir+'/*'):
            if len(glob.glob(subdir+"/output*.json.gz")) > 0:
                dirs.append(subdir)

    return get_all(dirs)

class Site(object):
    def __init__(self):
        self.samples = collections.defaultdict(list)

    def report(self, row):
        p = (d["local.radius"], d["local.cover"], d["species"], d["fragment.radius"])
        self.samples[row["sample.radius"]].append(p)

class Replicate(object):
    def __init__(self):
        self.sites = collections.defaultdict(Site)

    def report(self, row):
        self.sites[row["site.id"]].report(row)

    def to_dict(self):
        return [{ "site.id" : key, "samples" : self.sites[key].samples} for key in self.sites.keys()]

class Landscape(object):
    def __init__(self):
        self.replicates = collections.defaultdict(Replicate)

    def report(self, row):
        self.replicates[row["case.id"]].report(row)

    def to_dict(self):
        return [{ "case.id" : key, "sites" : self.replicates[key].to_dict() } for key in self.replicates.keys()]

def to_dict(dataset):
    landscapes = []
    for key in dataset.keys():
        l = {
            "landscape.fragments" : key[0],
            "landscape.cover" : key[1],
            "replicates" : dataset[key].to_dict()
        }
        landscapes.append(l)

    return landscapes

data = collections.defaultdict(Landscape)
data_itr = get_iterator()
for d in data_itr:
    s = (d["landscape.fragments"], d["landscape.cover"])
    data[s].report(d)

d = to_dict(data)
json.dump(d, sys.stdout)
