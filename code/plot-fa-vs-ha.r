library(ggplot2)
library(dplyr)
library(data.table)
library(scales)

my_colors_1 <- c("orange", "#4575b4")
my_colors <- c("#d73027","#fc8d59", "#fee090","#7fbf7b","#91bfdb","#4575b4", "#000000")


site_files <- c(
"local-experiments/1A-passive-d1-s32-u100/focal.csv.gz",
"local-experiments/1B-passive-d3-s32-u100/focal.csv.gz",
"local-experiments/1C-passive-d10-s32-u100/focal.csv.gz",
"local-experiments/2A-hostile-d1-s32-u100/focal.csv.gz",
"local-experiments/2B-hostile-d3-s32-u100/focal.csv.gz",
"local-experiments/2C-hostile-d10-s32-u100/focal.csv.gz",
"local-experiments/3A-active-d1-s32-u100/focal.csv.gz",
"local-experiments/3B-active-d3-s32-u100/focal.csv.gz",
"local-experiments/3C-active-d10-s32-u100/focal.csv.gz"
)

read_data <- function(files) {
    data <- data.frame()

    deltas <- mapply(function(x) { paste("delta==",x)}, c(1,3,10,
                1,3,10,
                1,3,10))

#    opts <- c(5,8,13,
#              5,8,13,
#              5,8,15) 

     opts <- c(5,10,15,
              5,10,15,
              5,10,15) 

    modes <- c("Passive-Habitable", "Passive-Habitable", "Passive-Habitable",
               "Passive-Hostile", "Passive-Hostile", "Passive-Hostile",
               "Active-Habitable", "Active-Habitable", "Active-Habitable")

    i = 1
    for (f in files[1:length(files)]) {
        input_data <- read.csv(f)
        input_data$case <- as.factor(f)
        input_data$delta_val <- as.factor(deltas[i])
        input_data$mode_val <- as.factor(modes[i])

        input_data$prior.opt.radius <- opts[i]

        print(f)
        print(summary(input_data))

        #print(summary(input_data))
        data <- rbind(data, input_data)
        i <- i + 1
    }
    data$mode_val <- factor(data$mode_val, levels = c("Passive-Habitable","Passive-Hostile", "Active-Habitable"))
    data
}

get_theme <- function() {
        theme(legend.position='bottom',
              legend.background=element_rect(size=0, fill = "transparent", colour = "transparent"),
              legend.key=element_rect(fill = "transparent", colour = "transparent"),
              legend.key.height = unit(0.6,'lines'),
              legend.key.width = unit(0.8, 'lines'),
              legend.title = element_text(size=8),
              legend.text = element_text(size=7),
              legend.margin = margin(0),
              plot.margin = margin(0),
              panel.border = element_rect(fill=NA,color="black"),
              axis.text=element_text(size=7),
              axis.title=element_text(size=8),
              axis.line = element_blank(),
              strip.background = element_blank(),
              strip.text = element_text(size=8, hjust=0))
}


gof <- function(m) {
    1 - m$deviance/m$null.deviance
}



data <- read_data(site_files)
data$fragment.area <- data$focal_radius**2.0 * pi

df_ll <- data %>% group_by(local.radius, mode_val, delta_val) %>% 
      do(data.frame(deviance = gof(glm(species ~ log(local.habitat), data=., family=poisson(link=log))))) %>%
      mutate(fitted.model="LL")
  
df_fll <- data %>% group_by(local.radius, mode_val, delta_val) %>% 
      do(data.frame(deviance = gof(glm(species ~ log(fragment.area) + log(local.habitat), data=., family=poisson(link=log))))) %>%
      mutate(fitted.model="F+LL") 

df_f <- data %>% group_by(local.radius, mode_val, delta_val) %>% 
      do(data.frame(deviance = gof(glm(species ~ log(fragment.area), data=., family=poisson(link=log))))) %>%
      mutate(fitted.model="F") 

opts <- df_ll %>% ungroup() %>% group_by(mode_val, delta_val, fitted.model) %>% filter(deviance==max(deviance)) %>% rename(opt.radius=local.radius)

ref <- df_f %>% group_by(delta_val, mode_val) %>% 
                mutate(ref.fa.line = deviance) %>%
                select(-c(deviance,fitted.model,local.radius))

dd <- rbind(df_ll,df_fll) %>% group_by(delta_val, mode_val) %>% 
               full_join(opts) %>%
               full_join(ref) %>%
               filter(fitted.model != "F") 
           
# Plot appropriate scale
p <- ggplot(dd, 
            aes(x=local.radius, y=deviance, color=as.factor(fitted.model))) +
    scale_colour_manual(name="Model", 
                        values=my_colors_1, 
                        labels=c("Fragment area and local landscape", "Local landscape")) + 
    geom_vline(data=filter(dd, fitted.model=="LL"), 
               aes(xintercept=opt.radius), linetype="dashed", color="black") + 
    geom_hline(aes(yintercept=ref.fa.line), 
               linetype="dashed", color="black") + 
    geom_line(size=1) +
    facet_grid(delta_val ~ mode_val, labeller=label_parsed) +
    theme_classic() +
    xlab("Radius of the focal landscape") + 
    ylab(expression("Pseudo "~R^2)) + 
    geom_text(data=filter(dd, fitted.model=="LL"), aes(x=opt.radius+4, label=opt.radius, y=0.01), size=3) +
    get_theme()

pdf("local-experiments/local-opt-landscapes.pdf")
print(p)
dev.off()

# Plot log species vs log focal area
fdata <- data %>% group_by(local.radius, delta_val, mode_val) %>%
                  full_join(opts %>% ungroup() %>% filter(fitted.model=="LL") %>% select(-fitted.model)) 

sf <- fdata %>% filter(local.radius==opt.radius) %>%
                group_by(focal_patch_area, local_habitat_area, delta_val, mode_val) %>% 
                summarise(avg.species=mean(species), focal.area=mean(focal_patch_area))

p2 <- ggplot(sf, aes(x=log2(focal.area), y=log2(avg.species), color=as.factor(local_habitat_area))) + 
      geom_point() + geom_line() + theme_classic() + get_theme() + 
      facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
      xlab(expression("log"[2]~"focal patch area")) +
      ylab(expression("log"[2]~"average species")) 

pdf("local-experiments/local-s-vs-focal.pdf")
print(p2)
dev.off()


# Plot log species vs local habitat
hf <- fdata %>% filter(local.radius==prior.opt.radius, local_radius*100==local.radius) %>%
                group_by(delta_val, mode_val, focal_patch_area, local_habitat_area) %>%
                summarise(species=mean(species), habitat=mean(local_habitat_area))

p3 <- ggplot(hf, aes(x=log2(habitat),y=log2(species), color=as.factor(focal_patch_area))) + 
      theme_classic() + get_theme() + 
      facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
      geom_point() + 
      geom_line() + 
    #  geom_smooth(method=glm) + 
      xlab(expression("log"[2]~"amount of habitat in local landscape")) +
      ylab(expression("log"[2]~"average species")) 

pdf("local-experiments/local-s-vs-ha.pdf")
print(p3)
dev.off()
