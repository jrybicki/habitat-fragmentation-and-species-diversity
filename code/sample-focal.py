import json
import sys
import random

data = json.load(sys.stdin)
random.seed(int(sys.argv[1]))

print "landscape.fragments,landscape.cover,replicate.id,site.id,sample.radius,local.radius,fragment.radius,species,local.cover"
for landscape in data:
    for replicate in landscape["replicates"]:
        site = random.choice(replicate["sites"])
        for (sample_radius, samples) in site["samples"].items():
            for sample in samples:
                local_radius, local_cover, species,fragment_radius = sample
                print "{},{},{},{},{},{},{},{},{}".format(landscape["landscape.fragments"], 
                                                 landscape["landscape.cover"],
                                                 replicate["case.id"],
                                                 site["site.id"],
                                                 sample_radius,
                                                 local_radius,
                                                 fragment_radius,
                                                 species,
                                                 local_cover)

