import json
import csv
import sys
import collections
import matplotlib.pyplot as pp
import argparse
import itertools as it

parser = argparse.ArgumentParser()
parser.add_argument('community', help='Community file')
parser.add_argument('density', help='Initial density')
parser.add_argument('U', type=float, help="domain size")
parser.add_argument('-p', '--postdensity', help='After fragmentation density', default=None)
parser.add_argument('-o', '--output', help='Output file', default=None)
parser.add_argument('types', nargs='+')
args = parser.parse_args()

community = json.load(open(args.community))

def read_rows(fname): 
    d = {}
    for k in community["entities"].keys():
        for index, model_id in enumerate(community["entities"][k]):
            d[model_id] = (k,index)

    print d
    with open(fname) as tsvfile:
        reader = csv.DictReader(tsvfile, dialect='excel-tab')
        for row in reader:
            nrow = {}
            for (k,v) in row.items():
                if not k in ["time","events"]:
                    nrow[d[int(k)]] = v
                else:
                    nrow[k] = v
            yield nrow

def plot(rows1, rows2, U, entitytypes=["satiated"]):
    A = U*U
    xs = []
    ys = collections.defaultdict(list)
    max_y = 0
    rows1 = list(rows1)
    rows2 = list(rows2)
    max_row1_t = float(rows1[-1]["time"])
    for r in rows2:
        r["time"] = float(r["time"]) + max_row1_t

    for r in it.chain(rows1,rows2):
        xs.append(float(r["time"]))
        for k in sorted(r.keys()):
            if k[0] in entitytypes:
                density = float(r[k])/A
                max_y = max(max_y, density)
                ys[k].append(density)

    print xs
    print ys

    if len(rows2) > 0:
        max_x = rows2[-1]["time"]
    else:
        max_x = rows1[-1]["time"]

    pp.ylim(0, 2*max_y)
    pp.axvline(max_row1_t, color='black', linestyle='--')
    for k in sorted(ys.keys()):
        pp.plot(xs, ys[k], label="{}{}".format(k[0][0].upper(),k[1]))

    pp.legend(loc=1)

rows1 = read_rows(args.density)
if args.postdensity is not None:
    rows2 = read_rows(args.postdensity)
else:
    rows2 = []

plot(rows1, rows2, args.U, args.types)
if args.output is not None:
    pp.savefig(args.output)
else:
    pp.show()
