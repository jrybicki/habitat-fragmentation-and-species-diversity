library(ggplot2)
library(dplyr)
library(data.table)
library(scales)

source("common.r")

# Input arguments
args <- commandArgs(trailingOnly = TRUE)
outdir <- args[1]
opts_fname = paste(outdir, "focal-opts.csv", sep="/")
sar_fname = paste(outdir, "focal-sar.csv", sep="/")

plot_ll <- function(data, fname) {
    # Plot appropriate scale
    models <- c("F", "LL", "F+LL")
    y_max <- max(data$deviance)
    y_lim_max <- ceiling(y_max/.1)*.1
    y_breaks <- seq(0, y_lim_max, 0.1)

    p <- ggplot(data %>% filter(fitted.model %in% models), 
                aes(x=local.radius, y=deviance, color=as.factor(fitted.model))) +
        scale_colour_manual(name="Model", values=ll_colors, guide="none") + 
        geom_vline(data=filter(data, fitted.model=="LL"), 
                   aes(xintercept=opt.ll.radius), linetype="dashed", color="black", size=0.75) + 
        geom_line(size=1) +
        facet_grid(delta_val ~ mode_val) +
        theme_classic() +
        xlab(bquote("Radius of the focal landscape"~(tau==.(r)))) + 
        ylab(expression("Pseudo "~R^2)) + 
        geom_text(data=filter(data, fitted.model=="LL"), aes(x=opt.ll.radius+6, label=opt.ll.radius, y=0.01), size=3) +
        get_theme() +
        scale_y_continuous(breaks=y_breaks) + ylim(0, y_lim_max)

    pdf(fname, width=3.5, height=3.5)
    print(p)
    dev.off()
}

plot_sar <- function(data, fname) {
    # Plot appropriate scale
    p <- ggplot(data, aes(x=log2(local.cover), y=log2(species))) + 
        geom_point(size=0.5) +
        facet_grid(delta_val ~ mode_val, labeller=label_parsed) +
        xlab(expression("log"[2]~"local cover")) + 
        ylab(expression("log"[2]~"#species")) + 
        stat_smooth(formula = y ~ s(x, k = 10), method = "gam", se = FALSE)+ 
        theme_classic() +
        get_theme()

    pdf(fname, width=5, height=5)
    print(p)
    dev.off()
}

plot_fa <- function(data, fname) {
    # Plot appropriate scale

    p <- ggplot(data, aes(x=log2(fragment.area), y=log2(species))) + 
        geom_point(size=0.5) +
        facet_grid(delta_val ~ mode_val, labeller=label_parsed) +
        xlab(expression("log"[2]~"area of local fragment")) + 
        ylab(expression("log"[2]~" #species")) + 
        geom_smooth(color="red", size=1.5) + 
        theme_classic() +
        get_theme()

    pdf(fname, width=5, height=5)
    print(p)
    dev.off()
}

aic_table <- function(da) {
    modes=unique(da$mode_val)
    deltas=unique(da$delta_val)
    results = matrix(NA,nrow = 9,ncol=9)

    c=0
    rnames = rep("xx",9)
    for (m in 1:3){
      for (de in 1:3){
        c=c+1
        sel = da$mode_val==modes[m] & da$delta_val==deltas[de]
        da1=da[sel,]
        m12 = glm(species~log(local.habitat) + log(fragment.area),data=da1,family = poisson(link=log))
        m1 = glm(species~log(local.habitat), data=da1,family = poisson(link=log))
        m2 = glm(species~log(fragment.area), data=da1,family = poisson(link=log))
        m0 = glm(species~1,data=da1,family = poisson(link=log))
        aic=AIC(m12,m1,m2,m0)
        aic$DAIC = aic$AIC-min(aic$AIC)
        results[c,1] = deltas[de]
        results[c,2:3] = m12$coefficients[2:3]
        results[c,4] = m1$coefficients[2]
        results[c,5] = m2$coefficients[2]
        results[c,6:9]=aic$DAIC[1:4]
        rnames[c] = as.character(modes[m])
      }
    }
    results = round(results, 2)
    colnames(results)=c("delta","beta L (M.L+F)","beta F (M.L+F)","beta L (M.L)","beta F (M.F)","AIC M.L+F","AIC M.L","AIC M.F","AIC M.null")
    rownames(results)=rnames
    results
}

ll <- read.csv(opts_fname)
sar <- read.csv(sar_fname)

ll$mode_val <- lapply(ll$mode_val,get_long_mode_name)
ll$mode_val <- factor(ll$mode_val, levels = c("Passive-Hostile", "Passive-Habitable", "Active-Habitable"))

ll_fname_prefix = paste(outdir, "focal-", sep="/")
sar_fname_prefix = paste(outdir, "focal-sar-", sep="/")
fa_fname_prefix = paste(outdir, "focal-fa-", sep="/")
table_fname_prefix = paste(outdir, "focal-aic-", sep="/")

print(ll_fname_prefix)

# Make plots for each sample radii
for (r in unique(ll$sample.radius)) {
    plot_ll(ll %>% filter(sample.radius == r), paste(ll_fname_prefix, r, ".pdf", sep=""))
}

# Analyse SAR data
for (r in unique(sar$sample.radius)) {
    sar_data <- sar %>% filter(sample.radius == r)
#    plot_sar(sar_data, paste(sar_fname_prefix, r, ".pdf", sep=""))
#    plot_fa(sar_data, paste(fa_fname_prefix, r, ".pdf", sep=""))
    res <- aic_table(sar_data)
    write.csv(res, paste(table_fname_prefix, r, ".csv", sep=""))
}
#warnings()
