#!/bin/bash
./run-illustration-sim.sh illustrations/illustration-1-gradient/ 1 0 115177458227 120638553997 1 1
./run-illustration-sim.sh illustrations/illustration-2-gradient/ 10 0 155177458227 16638553997 1 1
./run-illustration-sim.sh illustrations/illustration-1/ 1 0 15177458227 20638553997 1 0 
./run-illustration-sim.sh illustrations/illustration-2/ 10 0 55177458227 6638553997 1 0
mkdir -p illustrations/animations
for f in ./illustrations/illustration-*/*.gif; do
    case=$(dirname $f | cut -d'/' -f 3 | cut -d'-' -f 2,3)
    new="illustrations/animations/$case-$(basename $f)"
    gifsicle -i $f -o $new -O2 --resize-width 400 --resize-method catrom --colors 32
done
