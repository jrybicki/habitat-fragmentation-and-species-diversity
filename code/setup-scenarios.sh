#!/bin/bash
./setup.py --U 100 --T0 300 --T1 300 --R 100 --S 64 --seed 123456789 --scenario basic --outdir experiments/basic-100-64
./setup.py --U 100 --T0 300 --T1 300 --R 100 --S 64 --seed 987654321 --scenario gradient --outdir experiments/gradient-100-64
./setup.py --U 50 --T0 300 --T1 300 --R 10 --S 32 --seed 123456789 --scenario basic --outdir experiments/basic-50-32
./setup.py --U 50 --T0 300 --T1 300 --R 10 --S 32 --seed 987654321 --scenario gradient --outdir experiments/gradient-50-32
./setup.py --U 25 --T0 10 --T1 10 --R 5 --S 16 --seed 666 --scenario basic --outdir experiments/test-25-16

