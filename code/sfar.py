from sloss import *

def compute_sar(data, order=0):
    res = collections.defaultdict(list)
    vals = collections.defaultdict(list)

    for basedir in data:
        indata = json.load(util.open_file("{}/output-1.0.json.gz".format(basedir)))
        community = json.load(util.open_file("{}/community.json".format(basedir)))
        entity_map = get_entity_map(community)
        species_counts = get_type_count("satiated", indata["data"][-1]["total.counts"], entity_map)
        vals["species"].append(hill(species_counts, 0))
        vals["hill.1"].append(hill(species_counts, 1))
        vals["hill.2"].append(hill(species_counts, 2))
        vals["individuals"].append(sum(species_counts))

    for key in vals.keys():
        res[key] = numpy.average(vals[key])

    return res

def get_sfar_data(data):
    summary = {}
    for p in data.keys():
        summary[p] = compute_sar(data[p])
    return summary

def write_sfar_csv(sfar_data, sfar_data_fname):
    w = csv.DictWriter(open(sfar_data_fname, 'w'), fieldnames = { 'fragments', 'species', 'cover', "hill.1", "hill.2", "individuals" })
    w.writeheader()
    for p,vals in sfar_data.items():
        fragments, cover = p
        vals["cover"] = cover
        vals["fragments"] = fragments
        w.writerow( vals )

if __name__ == '__main__':
    d = read_data(sys.argv[1])
    sfar_data_fname = "{}/data-sfar.csv".format(sys.argv[2])
    sfar_data = get_sfar_data(d)
#    print(sfar_data)
    write_sfar_csv(sfar_data, sfar_data_fname)
