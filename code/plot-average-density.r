require(ggplot2)
require(reshape2)
require(dplyr)
source("common.r")

# Arguments
args <- commandArgs(trailingOnly = TRUE)
outdir <- args[1]
casedirs <- args[-1]

data_files <- paste_filename_to_subdirs("density.csv", casedirs)

data <- read_suite_data(data_files)

dd <- data %>% 
     filter(landscape.cover >= 0.01) %>%
     select(time, mode_val, delta_val, landscape.fragments, landscape.cover, replicate.id, starts_with("S")) %>%
     melt(id.vars=c("time", "mode_val", "delta_val", "landscape.cover", "landscape.fragments", "replicate.id")) %>%
     mutate(time = as.integer(time))

for(f in unique(dd$landscape.fragments)) {
    d <- dd %>% filter(landscape.fragments == f)

    # Compute average over all replicates
    sd <- d %>%
        group_by(time, mode_val, delta_val, landscape.fragments, landscape.cover) %>%
        summarise(species.density = mean(value))

    # Compute average in a single replicate
    ed <- d %>%
        filter(replicate.id == 19) %>%
        group_by(time, delta_val, mode_val, landscape.fragments, landscape.cover) %>%
        summarise(y=mean(value))

    fname <- sprintf("%s/density-%s-fragments.pdf", outdir, f)
    pdf(fname, width=4.2, height=4.2)
      p <- ggplot(sd,
            aes(x=time, y=log2(species.density),
                color=as.factor(landscape.cover))) +
          theme_classic() + 
          get_theme() +
          geom_line(size=1.5, alpha=0.4, linetype="solid") +
          geom_line(data=ed, aes(x=time, y=log2(y), color=as.factor(landscape.cover)), linetype="solid", size=0.5) +
          ylab("Log of satiated density") +
          scale_color_discrete(name="Landscape cover", labels=c("1%", "2%", "4%", "8%", "16%", "32%")) +
          facet_grid(delta_val ~ mode_val, labeller=label_parsed) 
      print(p)
    dev.off()
}
