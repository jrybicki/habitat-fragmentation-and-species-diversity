import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.image as mplimage 
from matplotlib.backends.backend_pdf import PdfPages

import sys
import util
import glob
import math

basedir = sys.argv[1]
pdf_name = sys.argv[2]

inputs = list(util.find_recursive(basedir, "snapshot.png"))

print inputs

mpl.rcParams['mathtext.fontset'] = 'cm'
mpl.rcParams.update({'font.size': 8})

pdf = PdfPages( pdf_name )
fig = plt.figure(figsize=(10,2), dpi=400)
gs = gridspec.GridSpec(1, 5)
gs.update(wspace=0.025, hspace=0.05) 

for g, fname in zip(gs,inputs):
    img = mplimage.imread(fname)
    print g, fname
    ax = plt.subplot(g)
    ax.imshow(img,interpolation='none')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')
    ax.set_aspect('equal')

    if fname.find("/init/") >= 0:
        title ="$N=1$, $C=1$"
    else:
        fragments, cover = fname.split("/")[-3].split("-")
        title ="$N={}$, $C={}$".format(fragments,cover) 
    ax.set_title(title)

pdf.savefig(bbox_inches='tight')
pdf.close()
