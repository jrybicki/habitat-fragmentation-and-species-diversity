import copy
import sys
import json
import random
import argparse

import community_input 
import combinations as cmb
import util

# types:
# - competitive (default)
# - dispersal range 
# - dispersal type



defaults = {
#    "initial.immigration.multiplier" : 100,
    "initial.hungry.density" : 0.1,
    "immigration" : 0.001,
    "mortality" : 1,
    "hunger" : 0.1,
    "consumption" : [{ "resource" : 0, "scale" : 0.5, "rate" : 1 }], # 1 -> 0.5
    "resource.rate" : 4,
    "patch.density" : 0.1,
    "patch.radius" : 2,
    "patch.turnover" : 0.1,
    "resource.decay" : 0.1,
    "birth.rate" : 1
}

simulator_defaults = {
    "time" : 100,
    "domain" : 50
}

def prepare_input(case, species_count=1, dispersal_scale=1.0, propagule_establishment=False, hops=1, gradient=False):

    birth_scale = dispersal_scale
    hungry_jumps = []
    if hops > 1:
        hungry_jumps.append(
            {
                "scale" : dispersal_scale/(hops-1)**0.5,
                "rate" : hops*case["mortality"]
            }
        )
        birth_scale = dispersal_scale/(hops-1)**0.5

    species_defaults = {
        "immigration" : case["immigration"],
        "immigration.only.to.fragments" : propagule_establishment,
        "mortality" : case["mortality"],
        "hunger" : case["hunger"],
        "consumption" : case["consumption"],
        "hungry.jump" : hungry_jumps,
        "satiated.jump" : [],
        "birth" : [ {
            "scale" : dispersal_scale,
            "rate" : case["birth.rate"],
            "only.fragment.establishment" : propagule_establishment
        }]
    }

    patch_birth = case["patch.density"] * case["patch.turnover"]
    patch_death = case["patch.turnover"]

    c = {
        "patches" : [ { 
            "gradient" : -1 if not gradient else 0,
            "birth" : patch_birth,
            "death" : patch_death,
            "generators" : [ { "resource" : 0, "scale" : case["patch.radius"], "rate" : case["resource.rate"] }],
            "successor" : -1
        }],
        "resources" : [{ "decay" : case["resource.decay"] }]
    }
    c["species"] = [ copy.deepcopy(species_defaults) for i in xrange(species_count) ] 

    if gradient == True:
        # Species consume different resource types
        for (i,sdata) in enumerate(c["species"]):
            sdata["consumption"][0]["resource"] = i % 4

        # Four types of resources and patches
        rd = { "decay" : case["resource.decay"] }
        pd = c["patches"][0]
        for i in xrange(0,3):
            c["resources"].append(copy.deepcopy(rd))
            p = copy.deepcopy(pd)
            p["gradient"] = i+1
            p["generators"][0]["resource"] = i+1
            c["patches"].append(p)
        for pd in c["patches"]:
            pd["generators"][0]["rate"] /= 4.0

    community = community_input.make_input(c)
    community["initial.densities"] = {
            "resources" : [0 for r in c["resources"]],
            "patches" : [case["patch.density"] for p in c["patches"]],
            "hungry" : [case["initial.hungry.density"]] * species_count,
            "satiated" : [0.0] * species_count
    }
    return community

def generate(**parameters):
    return prepare_input(defaults, **parameters)

def main():
    gradient = True
    input_data = {
        "species_count" : int(sys.argv[1]),
        "dispersal_scale" : float(sys.argv[2]),
        "propagule_establishment" : bool(int(sys.argv[3])),
        "hops" : int(sys.argv[4]),
        "gradient" : int(sys.argv[5])
    }
        
    print json.dumps(prepare_input(defaults, **input_data), indent=2)

if __name__ == '__main__':
    main()
