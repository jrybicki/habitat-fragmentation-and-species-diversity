#!/usr/bin/env python

import setup
import random
import sys

import util

scenario_parameters = {
    "neutral": [
        { 'tag' : '1A-hostile-d1',  'args' : "-d 1 -H 1 --establishment --no-gradient"},
        { 'tag' : '1B-hostile-d3',  'args' : "-d 3 -H 1 --establishment --no-gradient"},
        { 'tag' : '1C-hostile-d10', 'args' : "-d 10 -H 1 --establishment --no-gradient"},
        { 'tag' : '2A-passive-d1',  'args' : "-d 1 -H 1 --no-establishment --no-gradient"}, 
        { 'tag' : '2B-passive-d3',  'args' : "-d 3 -H 1 --no-establishment --no-gradient"},
        { 'tag' : '2C-passive-d10', 'args' : "-d 10 -H 1 --no-establishment --no-gradient"},
        { 'tag' : '3A-active-d1',  'args' : "-d 1 -H 10 --no-establishment --no-gradient"},
        { 'tag' : '3B-active-d3',  'args' : "-d 3 -H 10 --no-establishment --no-gradient"},
        { 'tag' : '3C-active-d10', 'args' : "-d 10 -H 10 --no-establishment --no-gradient"}
    ],
    "gradient": [
        { 'tag' : '1A-hostile-d1',  'args' : "-d 1 -H 1 --establishment --gradient"},
        { 'tag' : '1B-hostile-d3',  'args' : "-d 3 -H 1 --establishment --gradient"},
        { 'tag' : '1C-hostile-d10', 'args' : "-d 10 -H 1 --establishment --gradient"},
        { 'tag' : '2A-passive-d1',  'args' : "-d 1 -H 1 --no-establishment --gradient"}, 
        { 'tag' : '2B-passive-d3',  'args' : "-d 3 -H 1 --no-establishment --gradient"},
        { 'tag' : '2C-passive-d10', 'args' : "-d 10 -H 1 --no-establishment --gradient"},
        { 'tag' : '3A-active-d1',  'args' : "-d 1 -H 10 --no-establishment --gradient"},
        { 'tag' : '3B-active-d3',  'args' : "-d 3 -H 10 --no-establishment --gradient"},
        { 'tag' : '3C-active-d10', 'args' : "-d 10 -H 10 --no-establishment --gradient"}
    ]
}


def log(s):
    print "[{}]\t{}".format(sys.argv[0], s)

def emit(s):
    print(s)
    return util.run(s)

def prepare(scenario, rootdir,  base_seed, args=""):
    dirs = []
    cases = scenario_parameters[scenario]
    for case in cases:
        new_seed = random.randrange(1, 2**32-1)
        casedir = "{rootdir}/{tag}".format(rootdir=rootdir, tag=case['tag'])
        dirs.append(casedir)
        ret = emit("./setup.py {} --seed={} {} {}".format(casedir, new_seed, case['args'], args))
        if ret != 0:
            log("setup.py failed; stopping")
            sys.exit(1)

    batches = ["sequential.sh", "parallel.sh", "slurm.sh"]
    for b in batches:
        fname = '{rootdir}/{batch}'.format(rootdir=rootdir, batch=b)
        log(fname)
        with open(fname, 'w') as outf:
            for d in dirs:
                outf.write("{casedir}/{batch}\n".format(casedir=d, batch=b))

    return dirs

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print "Usage: {} [scenario] [rootdir] [seed] [simulation arguments]".format(sys.argv[0])
        sys.exit(0)

    scenario = sys.argv[1]
    rootdir = sys.argv[2]
    seed = sys.argv[3]
    args = " ".join(sys.argv[4:])
    dirs = prepare(scenario, rootdir, seed, args)
    with open("{}/casedirs.txt".format(rootdir), 'w') as f:
        f.writelines((d + "\n" for d in dirs))

