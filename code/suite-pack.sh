#!/bin/bash
suitedir=$1
fname="$suitedir/casedirs.txt"
tmp="$suitedir/tmp.txt"
tarname="$suitedir/data.tar.gz"

if [ -f "$fname" ]; then
    echo "$fname exists. Packing data"
    echo $fname > $tmp
    while read f ; do
        for f2 in $f/*.{json,csv,pdf,json.gz,csv.gz}; do 
            if [ -f $f2 ]; then
                echo $f2 >> $tmp
            fi
        done
    done <$fname
    tar -cvzf $tarname -T $tmp
else
    echo "$fname does not exist. Is $suitedir a suite experiment directory?"
    exit 1
fi
