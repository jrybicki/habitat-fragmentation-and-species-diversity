#define CATCH_CONFIG_MAIN  
#include "catch.hpp"

#include "../pp.h"
#include "../fragments.h"

/**
 * Some basic tests for the data structures 
 **/


rng_t rng_instance;
std::vector<double> random_values(double max, int n) {
    std::vector<double> vs;
    for(int i = 0; i<n; i++) {
        vs.push_back(uniform_distribution(rng_instance) * max);
    }
    return vs;
}

using namespace pp;


TEST_CASE( "box", "[box]" ) {
    SECTION("Test box") {
        Box b(0.1, 1.0, 0.1, 1.0);
        Box b2(0.5, 0.9, 0.5, 0.9);
        Box b3(0.0, 2.0, 0.0, 2.0);
        Box b4(2.0, 3.0, 2.0, 3.0);

        REQUIRE( b.intersects(b2) );
        REQUIRE( b2.intersects(b) );
        REQUIRE( !b4.intersects(b) );
        REQUIRE( !b.intersects(b4) );

        REQUIRE( b.intersects(b3) );
        REQUIRE( b3.intersects(b) );
        REQUIRE( !b4.intersects(b) );
        REQUIRE( !b.intersects(b4) );

        REQUIRE( b2.intersects(b3) );
        REQUIRE( b3.intersects(b2) );
        REQUIRE( !b4.intersects(b) );
        REQUIRE( !b.intersects(b4) );
    }

    SECTION("Test line intersections") {
        Box b(0, 10, 0, 10);
        Box b2(-11, 3, 1, 15);
        REQUIRE( b.line_intersects(0, 10, -11, 3) == true );
        REQUIRE( b.intersects(b2) == true );
        REQUIRE( b2.intersects(b) == true );
    }
}
    

TEST_CASE( "circle", "[circle]" ) {

    SECTION("Test circle") {
        Circle c(Coord(1.0,1.0),1);
        Coord p(1.0,1.0);
        REQUIRE( c.contains(p) );
        Coord q(0.0,1.5);
        REQUIRE( !c.contains(q) );

        Box b(0.0, 2.0, 0.0, 2.0);
        REQUIRE( c.intersects(b) ); // b contains c

        Box b2(0.0, 1.1, 0.0, 1.1); 
        REQUIRE( c.intersects(b2) ); // b2 intersects with c
    }
}

TEST_CASE( "cover query", "[CoverLookup]" ) {

    SECTION("Construction") {
        Coord U(10.0, 10.0);
        std::vector<Circle> circles;
        circles.push_back(Circle(1.0, 1.0, 1.0));
        circles.push_back(Circle(5.0, 5.0, 0.5));
        CoverLookup cq(circles, 10, U);

        std::cout << "CQ Constructed" << std::endl;

        SECTION("Test queries") {
            REQUIRE( cq.is_covered(Coord(1.0,1.0)) );
            REQUIRE( !cq.is_covered(Coord(1.0, 9.99)) );
            REQUIRE( !cq.is_covered(Coord(9.99, 1.0)) );
            REQUIRE( !cq.is_covered(Coord(9.99, 9.99)) );

            REQUIRE( !cq.is_covered(Coord(0.2,0.2)) );
            REQUIRE( cq.is_covered(Coord(1.3,0.8)) );
            REQUIRE( !cq.is_covered(Coord(0.0,0.0)) );
            REQUIRE( cq.is_covered(Coord(4.9,5.1)) );
            REQUIRE( !cq.is_covered(Coord(3.0,3.0)) );
            REQUIRE( !cq.is_covered(Coord(9.9,9.9)) );
        }
        
    }

    SECTION("Test overflowing borders (torus)") {
        Coord U(10.0, 10.0);
        
        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(0.0, 0.0, 1.0));
            CoverLookup cq(circles, 10, U);

            REQUIRE( cq.is_covered(Coord(0.0,0.0)) );
            REQUIRE( cq.is_covered(Coord(9.99,0.0)) );
            REQUIRE( cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( cq.is_covered(Coord(9.99,9.99)) );

            REQUIRE( !cq.has_within(Coord(5.0, 5.0), 4.0) );
            REQUIRE( cq.has_within(Coord(1.0, 1.0), 1.0) );
            REQUIRE( cq.has_within(Coord(9.0, 9.0), 1.0) );
            REQUIRE( !cq.has_within(Coord(1.0, 8.999), 1.0) );
        }

        SECTION("Test queries 1") {
            std::vector<Circle> circles;
            circles.push_back(Circle(1.0, 5.0, 2.0));
            CoverLookup cq(circles, 10, U);


            REQUIRE( cq.is_covered(Coord(1.0, 5.0)) ); // center
            REQUIRE( cq.is_covered(Coord(1.0, 6.0)) ); // near
            REQUIRE( cq.is_covered(Coord(1.0, 4.0)) ); // near
            REQUIRE( cq.is_covered(Coord(2.3, 5.5)) ); // near
            REQUIRE( cq.is_covered(Coord(2.0, 5.0)) ); // near

            REQUIRE( cq.is_covered(Coord(1.0, 3.0)) ); // above
            REQUIRE( cq.is_covered(Coord(1.0, 7.0)) ); // below
            REQUIRE( cq.is_covered(Coord(9.0, 5.0)) ); // otherside

            REQUIRE( !cq.is_covered(Coord(1.0, 2.99)) );
            REQUIRE( !cq.is_covered(Coord(1.0, 7.01)) );
            REQUIRE( !cq.is_covered(Coord(8.99, 5.0)) );
        }

        SECTION("Test queries 2") {
            std::vector<Circle> circles;
            circles.push_back(Circle(7.0, 1.0, 2.0));
            CoverLookup cq(circles, 10, U);

            REQUIRE( cq.is_covered(Coord(7.0, 1.0)) ); // above
            REQUIRE( cq.is_covered(Coord(9.0, 1.0)) ); // below
            REQUIRE( cq.is_covered(Coord(8.0, 2.0)) ); // otherside

            REQUIRE( !cq.is_covered(Coord(1.0, 2.99)) );
            REQUIRE( !cq.is_covered(Coord(1.0, 7.01)) );
            REQUIRE( !cq.is_covered(Coord(8.99, 5.0)) );
        }



        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(9.99, 9.99, 1.0));
            CoverLookup cq(circles, 10, U);

            REQUIRE( cq.is_covered(Coord(0.0,0.0)) );
            REQUIRE( cq.is_covered(Coord(9.99,0.0)) );
            REQUIRE( cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( cq.is_covered(Coord(9.99,9.99)) );
        }
       
        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(0.0, 1.0, 1.0));
            CoverLookup cq(circles, 10, U);
            REQUIRE( cq.is_covered(Coord(0.0,1.0)) );
            REQUIRE( cq.is_covered(Coord(9.9,1.0)) );
            REQUIRE( !cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( !cq.is_covered(Coord(9.99,9.99)) );
        }

        
        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(9.9, 1.0, 1.0));
            CoverLookup cq(circles, 10, U);
            REQUIRE( cq.is_covered(Coord(0.0,1.0)) );
            REQUIRE( cq.is_covered(Coord(9.9,1.0)) );
            REQUIRE( !cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( !cq.is_covered(Coord(9.99,9.99)) );
        }
         
    }

    SECTION("Random input") {
        double U = 1000.0;
        int N = 1000;
        double tests = 5000;

        auto xs = random_values(U, N);
        auto ys = random_values(U, N);
        auto rs = random_values(U/4.0, N);
        std::vector<Circle> circles;
        for(int i = 0; i<N; i++) {
            circles.push_back(Circle(xs[i], ys[i], rs[i]));
        }

        CoverLookup cq(circles, 7, U);

        std::cout << "Running " << tests << " test cases" << std::endl;
        for(auto t = 0; t<tests; t++) {
            auto x = uniform_distribution(rng_instance) * U;
            auto y = uniform_distribution(rng_instance) * U;

            auto p = Coord(x,y);

            /* Test whether p is covered by some circle */
            bool covered = false;
            for(auto c : circles) {
                if (c.center.torus_squared_distance(p, U) <= c.radius*c.radius) {
                    covered = true;
                }
            }
            REQUIRE( covered == cq.is_covered(p) );

            /* Test that whether p intersects with some circle*/
            auto r = uniform_distribution(rng_instance) * U/2;
            bool within = false;
            for(auto c : circles) {
                double dist = (c.radius+r)*(c.radius+r);
                if (c.center.torus_squared_distance(p, U) <= dist) {
                    within = true;
                }
            }

            REQUIRE( within == cq.has_within(p, r) );
        }
    }

}

TEST_CASE( "approximate_cover", "[apx]" ) {

    SECTION("Construction") {
        rng_t rng; 
        rng.seed(123456789);
        double U = 100.0;
        Coord Uc(U, U);

        SECTION("Test cover approximation") {
            
            std::vector<Circle> circles;
            circles.push_back(Circle(1.0, 1.0, 1.0));
            circles.push_back(Circle(5.0, 5.0, 0.5));
            CoverLookup cq(circles, 10, Uc);

            Circle c1 = Circle(1.0, 1.0, 1.0);
            Circle c2 = Circle(5.0, 5.0, 1.0);

            for(int i = 1; i<100; i++) {
               REQUIRE( (approximate_circle_cover(cq, c1, U, i, rng) == 1.0) );

            }

           REQUIRE( (approximate_circle_cover(cq, c2, U, 10000, rng) > 0.24) );
           REQUIRE( (approximate_circle_cover(cq, c2, U, 10000, rng) < 0.26) );
        }

        SECTION("Test corners") {
            std::vector<Circle> circles;
            circles.push_back(Circle(0.0, 0.0, 10.0));
            CoverLookup cq(circles, 10, Uc);

            Circle c1 = Circle(0.0, 0.0, 1.0);            
            
            for(double r = 0; r<10; r++) {
                for(int i = 1; i<1000; i++) {
                    double epsilon = 0.0001;
                    auto c2 = Circle(0.0, 0.0, r);
                    auto c3 = Circle(U-epsilon, U-epsilon, r);
                    auto c4 = Circle(0.0, U-epsilon, r);
                    auto c5 = Circle(U-epsilon, 0.0, r);
                    REQUIRE( approximate_circle_cover(cq, c2, U, i, rng) == 1.0 );                
                    REQUIRE( approximate_circle_cover(cq, c3, U, i, rng) == 1.0 );                
                    REQUIRE( approximate_circle_cover(cq, c4, U, i, rng) == 1.0 );                
                    REQUIRE( approximate_circle_cover(cq, c5, U, i, rng) == 1.0 );                
                }
            }
        }
    }
}

TEST_CASE( "bad input", "[bi]") {

    SECTION("Test faulty case") {
        rng_t rng; 
        rng.seed(123456789);
        double U = 100.0;

        std::vector<Circle> circles;
        double x = 96; // 96.0067886419263;
        double y = 8; // 8.17133312474358 ;
        circles.push_back(Circle(x, y, 7.0));
        CoverLookup cq(circles, 10, Coord(100.0,100.0));

        REQUIRE( cq.is_covered( Coord(8.0, 0.0) ) == false ); // X and Y swapped?!
        REQUIRE( cq.is_covered( Coord(0.0, 8.0) ) == true );

        for(double r = 0; r<7; r++) {
            auto c2 = Circle(x, y, r);
            REQUIRE( approximate_circle_cover(cq, c2, U, 100, rng) == 1.0 );                
        }
    }
}

TEST_CASE("random centers", "[rc]") {
    SECTION("Random centers") {
        const auto repeats = 100;
        auto U = 100.0;
        auto N = 10;
        for(auto k = 0; k<repeats; k++) {
            std::vector<Circle> circles;
            
            auto xs = random_values(U, N);
            auto ys = random_values(U, N);
            auto rs = random_values(U/2.0, N);

            for(auto i = 0; i<N; i++) {
                circles.push_back(Circle(xs[i], ys[i], rs[i]));
            }

            CoverLookup cq(circles, 10, Coord(U,U));
            for(auto i = 0; i<N; i++) {
                for(auto r = 0; r<rs[i]; r++) {
                    auto tmp = Circle(Coord(xs[i], ys[i]), r);
                    REQUIRE( approximate_circle_cover(cq, tmp, U, 100, rng_instance) == 1.0 );
                }
            }
        }

    }

}
