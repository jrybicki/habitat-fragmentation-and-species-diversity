#define CATCH_CONFIG_MAIN  
#include "catch.hpp"

#include "../pp.h"

/**
 * Some basic tests
 **/


rng_t rng;

using namespace pp;


TEST_CASE( "tophat", "[tophat]" ) {
    SECTION("Test samples") {
        for(auto radius = 1; radius<20; radius++) {
            auto k = pp::Tophat(1, radius);
            for(auto i = 0; i < 10000; i++) {

                // Sample within point
                auto p = k.sample(rng);
                REQUIRE( (p[0]*p[0]+p[1]*p[1]) < radius*radius );

                // Sample around a point without wrap
                auto c = Coord(5.0, 5.0);
                p = k.sample_around(rng, c);
                REQUIRE( ((p[0]-c[0])*(p[0]-c[0])+(p[1]-c[1])*(p[1]-c[1])) < radius*radius );

                // Sample with wraps
                auto U = 10.0;
                p = k.sample_around_w(rng, c, U);
                REQUIRE (p.torus_squared_distance(c, U) < radius*radius);

            }
        }
    }
}
    
