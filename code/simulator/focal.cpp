#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <list>

#include "json.hpp"
using json = nlohmann::json;

#include "cxxopts.hpp"

#include "ppsim/pp.h"
#include "ppsim/fragments.h"

rng_t rng;
unsigned long SAMPLES_PER_UNIT_AREA = 100;
signed long MAX_FRAGMENTS_TO_SAMPLE = -1; // set < 0 to sample all sites
signed long FOCAL_PATCH = -1; // < 0 means all

double MIN_RADIUS = 0; // 0 means no bound
signed int MAX_PATCHES = 0; // 0 means all

cxxopts::Options parse_args(int argc, char *argv[]) {
    try {
        cxxopts::Options options("ppsim", "Point process simulator");
        options.positional_help("[optional args]");
        options.add_options()
          ("h,help", "Print help")
          ("i,input", "Input file", cxxopts::value<std::string>())
          ("s,seed", "Seed", cxxopts::value<unsigned long>())
          ("a,accuracy", "Sampling accuracy", cxxopts::value<unsigned long>())
          ("f,focal", "Focal patch to sample", cxxopts::value<signed long>())
          ("n,subsample", "Number of samples", cxxopts::value<signed int>())
          ("r,minradius", "Minimum radius of focal fragment", cxxopts::value<double>())
          ;

        options.parse(argc, argv);

        if (options.count("help")) {
          std::cout << options.help({""}) << std::endl;
          exit(0);
        }
       
        return options;
    } catch (const cxxopts::OptionException& e) {
        std::cerr << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
}

pp::circle_container read_circles(json &input) {
    pp::circle_container cs;
    for(auto f : input["scaled.fragments"]) {
        pp::Circle c(pp::Coord(f["x"], f["y"]), f["r"]);
        cs.push_back(c);
    }
    return cs;
}

json analyse_site(json &site, pp::CoverLookup &cl, double U) {
    json results;
    double cx = site["x"];
    double cy = site["y"];
    for(int i = 1; i<=U; i+=2) {
        double radius = double(i)/2.0+0.5;
        auto sample_circle = pp::Circle(cx,cy,radius);
        auto sample_count = SAMPLES_PER_UNIT_AREA * sample_circle.area;
        double cover = pp::approximate_circle_cover(cl, sample_circle, U, sample_count, rng);
        json r;
        r["local.radius"] = radius;
        r["local.cover"] = cover;
        r["local.habitat"] = radius*radius*M_PI*cover;
        results.push_back(r);
    }
    return results;
}

json collect_data_from_sample_sites(json input) {
    auto input_cs = read_circles(input);
    double U = input["U"];
    auto cl = pp::CoverLookup(input_cs, 1, pp::Coord(U,U));

    json output;

    auto sites = input["sample.sites"];
    std::shuffle(sites.begin(), sites.end(), rng);
    
    auto count = -1;
    for (auto site : sites) {
        if (MAX_PATCHES > 0 && count > MAX_PATCHES) {
            break;
        }

        double pradius = -1;

        /* hack to get right radius */
        for (auto fragment : input["scaled.fragments"]) {
            if (fragment["x"] == site["x"] && fragment["y"] == site["y"]) {
                pradius = fragment["r"];
            }
        }

        if (MIN_RADIUS > 0 && pradius < MIN_RADIUS) {
            continue;
        }

        count += 1;
        std::vector<json> results = analyse_site(site, cl, U);

        json data;
        data["x"] = site["x"];
        data["y"] = site["y"];
        data["patch.radius"] = pradius;
        data["results"] = results;
        output["data"].push_back(data);
    }
    return output;
}

int main(int argc, char **argv) {
    auto options = parse_args(argc, argv);

    if (options.count("seed"))
        rng.seed(options["seed"].as<unsigned long>());

    if (options.count("accuracy"))
        SAMPLES_PER_UNIT_AREA = options["accuracy"].as<unsigned long>();

    if (options.count("subsample"))
        MAX_PATCHES = options["subsample"].as<signed int>();

    if (options.count("minradius"))
        MIN_RADIUS = options["minradius"].as<double>();



    /* Read input data */
    json input; // input from analysis
    std::cin >> input;

    auto output = collect_data_from_sample_sites(input);
    std::cout << output;

    return 0;
}
