#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <iomanip>
#include <cstdint>
#include <algorithm>

/* cxxopts library */
#include "cxxopts.hpp"

/* JSON library */
#include "json.hpp"
using json = nlohmann::json;

/* Simulator headers */
#include "ppsim/pp.h"
#include "ppsim/fragments.h"

using namespace pp;

/* Logging macro */
auto start_time = std::chrono::system_clock::now();
#define LOG(str) do { \
    auto current = std::chrono::system_clock::now(); \
    auto elapsed = std::chrono::duration<double>(current-start_time); \
    std::cout << std::fixed << std::setprecision(10) << elapsed.count() << " -- "; \
    std::cout << str << std::endl; } while( false )

using seed_t = uint64_t;

enum env_gradient_t {
    NONE = -1,
    G1 = 0,
    G2 = 1,
    G3 = 2,
    G4 = 3
};


double gradient(double x, double y, env_gradient_t gradient_parameter) {
    switch (gradient_parameter) {
        case G1: return (1+sin(2*M_PI*x))/2.0;
        case G2: return (1+sin(2*M_PI*x+M_PI))/2.0;
        case G3: return (1+sin(2*M_PI*y))/2.0;
        case G4: return (1+sin(2*M_PI*y+M_PI))/2.0;
        case NONE:
        default: return 1; break;
    }
}

Coord sample_gradient_location(SimulationState &s, env_gradient_t gradient_parameter) {
    if (gradient_parameter == NONE) {
        return s.random_coord();
    }

    while(true) {
        auto x = s.random_value();
        auto y = s.random_value();
        auto r = s.random_value();
        if (gradient(x,y, gradient_parameter) > r) {
            return Coord(x*s.U(), y*s.U());
        }
    }
}

class ImmigrationWithGradient : public Process<0,1> {
public:
    ImmigrationWithGradient(uint_t entity, double rate_parameter, env_gradient_t gradient_type) : Process<0,1>({}, {{entity}}, 0), rate(rate_parameter), gradient_parameter(gradient_type) {}
    
    double propensity(SimulationState &s) const { return rate * s.area(); }

    void activate(SimulationState &s, point_del_buf_t &removed, point_add_buf_t &added)  { 
        Coord c = sample_gradient_location(s, gradient_parameter);
        added.push_back(s.new_point(c, output(0)));
    }

protected:


    double rate;
    double radius;
    env_gradient_t gradient_parameter;
};

class ImmigrationWithEstablishment : public Process<0,1> {
public:
    ImmigrationWithEstablishment(uint_t entity, double rate_parameter, double radius_parameter, CoverLookup &fragment_lookup, env_gradient_t gradient_type = NONE) : Process<0,1>({}, {{entity}}, 0),  gradient_parameter(gradient_type), rate(rate_parameter), radius(radius_parameter), lookup(fragment_lookup) {}
    
    double propensity(SimulationState &s) const { return rate * s.area(); }

    void activate(SimulationState &s, point_del_buf_t &removed, point_add_buf_t &added)  { 
        auto c = sample_gradient_location(s, gradient_parameter);
        if (lookup.has_within(c, radius)) {
            added.push_back(s.new_point(c, output(0)));
        }
    }

protected:
    env_gradient_t gradient_parameter;
    double rate;
    double radius;
    CoverLookup &lookup;
};

template<typename K>
class BirthWithEstablishment : public Process<1,1> {
public:

    template<typename... Args>
    BirthWithEstablishment(uint_t parent, uint_t child, CoverLookup &fragment_lookup, const Args&... args) : Process<1,1>({{parent}},{{child}},0), lookup(fragment_lookup), kernel(K(args...)) { 
    }

    double propensity() const {
        return kernel.integral;
    }

    void activate(SimulationState &s, Point *p, point_del_buf_t &removed, point_add_buf_t &added)  { 
        auto target = kernel.sample_around_w(s.rng(), p->get_coord(), s.U());
        if (lookup.is_covered(target)) {
            added.push_back(s.new_point(target, output(0)));
        }
    }
private:
    CoverLookup &lookup;
    K kernel;
};


std::vector<Circle> read_fragments(const json &input, double U) {
    std::vector<Circle> circles;
    if (input.count("fragments") == 0) return circles; // empty
    for(auto f : input["fragments"]) {
        double x = f["x"];
        double y = f["y"];
        double r = f["r"];
        circles.push_back(Circle(x*U, y*U, r*U));
    }
    return circles;
}

/* MODEL SPEC */
Model get_model(const json &input, CoverLookup &fragment_lookup) {
    auto c = input.at("model");
    auto e = input.at("entities");
    auto m = Model();

    bool has_fragments = (input.count("fragments") && input["fragments"].size() > 0);

    /* Patch dynamics */
    for(auto i = 0; i<c["patches"].size(); i++) {
        auto p = c.at("patches").at(i);
        auto mid = e.at("patches").at(i); // model id
        
        int successor = p.at("successor");
        double death_rate = p.at("death");
        if (successor >= 0) {
            auto sid = e.at("patches").at(successor);
            m += ChangeInType(mid, sid, death_rate);
        } else {
            m + DensityIndependentDeath(mid, death_rate);
        }
        
        /* Generate resources */
        double max_radius = 0.0;
        for(auto g : p.at("generators")) {
            double scale = g.at("scale");
            max_radius = std::max(max_radius, scale);
            int r = g.at("resource");
            auto j = e.at("resources").at(r);
            if (has_fragments) {
                m += BirthWithEstablishment<Tophat>(mid,j,fragment_lookup,g.at("rate"),g.at("scale"));
            } else {
                m += Birth<Tophat>(mid,j,g.at("rate"),g.at("scale"));
            }
        }

        auto gradient = p.at("gradient");

        if (has_fragments) {
            m += ImmigrationWithEstablishment(mid, p.at("birth"), max_radius, fragment_lookup, gradient);
        } else {
            m += ImmigrationWithGradient(mid, p.at("birth"), gradient);
        }
    }

    /* Resource dynamics */
    for (auto j = 0; j<c.at("resources").size(); j++) {
        auto r = c.at("resources").at(j);
        m += DensityIndependentDeath(e.at("resources").at(j), r.at("decay"));
    }

    /* Species dynamics */
    for(auto i = 0; i<c["species"].size(); i++) {
        auto s = c.at("species").at(i);
        auto sid = e.at("satiated").at(i);
        auto hid = e.at("hungry").at(i);

        m += DensityIndependentDeath(hid, s.at("mortality"));
        m += ChangeInType(sid, hid, s.at("hunger"));

        if (has_fragments && s.count("immigration.only.to.fragments") && s.at("immigration.only.to.fragments")) {
            m += ImmigrationWithEstablishment(hid, s.at("immigration"), 0, fragment_lookup);
        } else {
            m += Immigration(hid, s.at("immigration"));
        }

        /* Consumption/utilisation kernels */
        for(auto u : s.at("consumption")) {
            int r = u.at("resource");
            auto rid = e.at("resources").at(r);
            m += ChangeInTypeByConsumption<Tophat>(hid, rid, sid, u.at("rate"), u.at("scale"));
        }

        /* Birth kernels */
        for(auto d : s["birth"]) {
            if (has_fragments && d.count("only.fragment.establishment") && d.at("only.fragment.establishment")) {
                m += BirthWithEstablishment<Gaussian>(sid, hid, fragment_lookup, d.at("rate"), d.at("scale"));
                LOG("BirthWithEstablishment");
            } else {
                m += Birth<Gaussian>(sid, hid, d.at("rate"), d.at("scale"));
            }
        }

        /* Movement kernels */
        for(auto j : s["hungry.jump"]) {
            m += Jump<Gaussian>(hid, j.at("rate"), j.at("scale"));
        }

        for(auto j : s["satiated.jump"]) {
            m += Jump<Gaussian>(sid, j.at("rate"), j.at("scale"));
        }
    }

    return m;
}

void fill_by_type(Simulator &sim, const json &input, std::string type) {
    if (input.count("initial.densities") == 0) return;

    auto densities = input.at("initial.densities");
    auto e = input.at("entities");

    if (densities.count(type) == 0) return;

    for (auto j = 0; j<densities.at(type).size(); j++) {
        double d = densities.at(type).at(j);
        auto t = e.at(type).at(j);
        sim.fill(t, d);
        LOG(type.c_str() << " #" << j << " filled with density " << d);
    }
}

void setup_state(Simulator &sim, const json &input) {
    fill_by_type(sim, input, "patches");
    fill_by_type(sim, input, "resources");
    fill_by_type(sim, input, "hungry");
    fill_by_type(sim, input, "satiated");
}

/* ----- main ----- */

cxxopts::Options parse_args(int argc, char *argv[]) {
    try {
        cxxopts::Options options("ppsim", "Point process simulator");
        options.positional_help("[optional args]");
        options.add_options()
          ("h,help", "Print help")
          ("t,time", "Simulation time", cxxopts::value<double>())
          ("S,step", "Simulation steps (overrides time)", cxxopts::value<int>())
          ("dt", "Step size for saving state",cxxopts::value<double>()->default_value("1.0"))
          ("U,domain", "Domain size", cxxopts::value<double>())
          ("i,input", "Input file", cxxopts::value<std::string>())
          ("m,model", "Model input file", cxxopts::value<std::string>())
          ("o,output", "Snapshot output file", cxxopts::value<std::string>())
          ("d,density", "Density file", cxxopts::value<std::string>())
          ("s,seed", "RNG seed", cxxopts::value<seed_t>())
          ("g,granularity", "Granularity (bucket width) of the pointset datastructure", cxxopts::value<double>()->default_value("1.0"))
          ("p,propensity", "Print propensity of initial configuration", cxxopts::value<bool>())
          ;

        //options.parse_positional({"input", "output", "positional"});
        options.parse(argc, argv);

        if (options.count("help")) {
          std::cout << options.help({""}) << std::endl;
          exit(0);
        }
       
        //auto v = options["positional"];
        // Specify required parameters
        //cxxopts::check_required(options, {"time"});
        
        return options;
    } catch (const cxxopts::OptionException& e) {
        std::cerr << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
}

int read_input_points(Simulator &s, std::ifstream in) { 
    int n = 0;
    std::array<coord_t,SimulationState::DIM> coords;
    uint_t entity;
    while(in >> entity) {
        for(uint_t i = 0; i<SimulationState::DIM; i++) {
            in >> coords[i];
            if (in.eof()) {
                throw std::runtime_error("Malformed input file");
            }
        }
        Coord c(coords);
        s.add_new_point(c, entity);
        n++;
    }

    assert(s.get_state().enumerate_copy().size() == n);
    return n;
}

#include <csignal> 


volatile sig_atomic_t SIG_INT_RECEIVED = 0;
void interrupt_handler(int) {
    SIG_INT_RECEIVED += 1;
    LOG("SIGINT received. Waiting for current step to finish...");
}

bool interrupt_received(const SimulationState &) {
    return SIG_INT_RECEIVED;
}

template<typename T>
T get_parameter(std::string name, json &defaults, cxxopts::Options &options) {
    bool opt_has = options.count(name);
    bool def_has = defaults.count(name);
    if (opt_has && def_has) {
        LOG("Overriding value '" << name << "' = " << defaults[name] << " in model json file with command line argument");
    }
    // cmd line arguments override defaults
    if (options.count(name)) {
        return options[name].as<T>();
    }
    if (defaults.count(name)) {
        return defaults[name];
    }
    LOG("Could not find a value for '" << name << "' in either model parameter file or command line arguments");
    exit(1);
}

bool is_set(std::string name, json &defaults, cxxopts::Options &options) {
    bool opt_has = options.count(name);
    bool def_has = defaults.count(name);
    return opt_has || def_has;
}

std::shared_ptr<std::fstream> open_output(std::string fname) {
    auto f = std::make_shared<std::fstream>(fname, std::ios::out);
    if (f->fail()) {
        LOG("Could not open file '" << fname << "': " << strerror(errno));
        exit(1);
    }
    return f;
}

int main(int argc, char *argv[]) {
#ifdef DEBUG
    LOG("DEBUG flag set");
#endif
#ifndef NDEBUG
    LOG("Asserts enabled");
#endif
    try {
        auto options = parse_args(argc, argv);

        auto dt = options["dt"].as<double>();

        LOG("Options parsed");

        /* Read model input */
        json json_model_input;
        if (options.count("model")) {
            auto modelfname = options["model"].as<std::string>();

            std::ifstream i(modelfname);
            LOG("Reading model input data from '" << modelfname << "'");
            i >> json_model_input;
            LOG("Input read");
        } else {
            LOG("No input model given. Reading STDIN");
            std::cin >> json_model_input;
        }

        auto defaults = json_model_input["simulator"];

        auto time = get_parameter<double>("time", defaults, options);
        double U = get_parameter<double>("domain", defaults, options);

        /* Construct the model */
        LOG("Constructing the model");
        LOG("Reading fragments..");
        auto cs = read_fragments(json_model_input, U);

        auto fragment_lookup = CoverLookup(cs, 1.0, Coord(U,U));

        auto m = get_model(json_model_input, fragment_lookup);
        m.done();
        LOG(m);
        

        /* Set up the simulator */
        LOG("Creating the simulator");
        double g =  options["granularity"].as<double>();
        auto s = Simulator(U,m,g);

        /* Register SIGINT handler for convenience */
        std::signal(SIGINT, interrupt_handler); 
        s.add_halting_condition(&interrupt_received);

        if (is_set("seed", defaults, options)) {
            auto seed = get_parameter<seed_t>("seed", defaults, options);
            //auto seed = std::stoull(seed_input); // in principle, we could convert input seed string in some other way for more entropy
            s.set_seed(seed);
            LOG("Using '" << seed << "' as seed");
        } 


        if (options.count("input")) {
            auto infname = options["input"].as<std::string>();
            LOG("Reading input configuration from '" << infname << "'");
            auto c = read_input_points(s, std::ifstream(infname));
            LOG(c << " input points read");
        } else {
            LOG("No input point configuration given");
            LOG("Setting up the initial state");
            setup_state(s, json_model_input);
            LOG("Initial state: " << s.get_state());
        }

        /* Open the snapshot output file */
        if (options.count("output")) {
            auto outfname = options["output"].as<std::string>();
            LOG("Output snapshots to '" << outfname << "' with dt=" <<dt);
            s.make_writer<SnapshotWriter>(open_output(outfname),dt);
        }

        /* Open the density output file */
        if (options.count("density")) {
            auto outfname = options["density"].as<std::string>();
            LOG("Output density to '" << outfname << "'");
            s.make_writer<DensityWriter>(open_output(outfname),dt);
        }

        if (options.count("propensity")) {
            LOG("Propensities:");
            double total = 0;
            for(const auto &t : s.model.get_trackers()) {
                auto prop = t->propensity();
                total += prop;
                LOG(t->get_process() << " = " << prop);
            }
            LOG("====== TOTAL: " << total);
        }

        if (options.count("step")) {
            int step_count = options["step"].as<int>();
            LOG("Executing " << step_count << " steps of simulation");
            for(int i = 0; i<step_count; i++) {
                s.step();
            }
        } else {
            LOG("Running the simulation for " << time << " time units");
            s.run(time);
            LOG("Simulation stopped at time " << s.get_state().stats.time << ". Halting reason: " << s.get_halt_reason());
        }
     } catch (const std::exception& e) {
        LOG("Exception encountered: " << e.what());
        return 1;
    }

    return 0;
}


