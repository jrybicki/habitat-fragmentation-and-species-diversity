#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <list>

#include "json.hpp"
using json = nlohmann::json;

#include "cxxopts.hpp"

#include "ppsim/pp.h"
#include "ppsim/fragments.h"

struct point_t {
    int type;
    double x, y;
}; 

struct snapshot_t {
    double time;
    int events;
    std::vector<point_t> points;
};

/**
 * Parse a single line.
 */
snapshot_t parse_line(std::string str) {
    std::istringstream ss(str);
    snapshot_t data;

    /* read header*/
    ss >> data.time;
    ss >> data.events;
    
    /* read point list */
    point_t p;
    while (ss >> p.type) {
        ss >> p.x;
        ss >> p.y;
        data.points.push_back(p);
    }

    return data;
}

class Reader {
    public:
    Reader(std::string fname) {
        U = 0.0;
        input_file = std::ifstream(fname);
        if (!input_file.is_open()) {
            throw std::exception();
        }
        has_next = true;

        /* Read U from header */
        getline(input_file, line_str);
        std::istringstream ss(line_str);
        std::string header;
        ss >> header;
        if (header != "U") {
            throw std::exception();
        }
        ss >> U;
    }

    bool prepare_next() {
        if (has_next) {
            has_next = bool(getline(input_file, line_str));
        }
        return has_next;   
    }

    snapshot_t get_next() {
        if (has_next) {
            return parse_line(line_str);
        }
        throw std::exception();
    }

    bool has_next;
    double U;
    std::string line_str;
    std::ifstream input_file;
};

// processing

using point_counts = std::vector<int>;

class snapshot_summary_t {
    public:
    point_counts total_counts; // count for the whole landscape
    std::vector<point_counts> fragment_counts; // count for each fragment
    std::vector<point_counts> sample_counts; // count for each sampled area

    snapshot_summary_t(int max_types, int max_fragments, int sample_site_count) {
        total_counts.resize(max_types, 0);
        fragment_counts.resize(max_fragments);
        sample_counts.resize(sample_site_count);

        for(int i = 0; i<max_fragments; i++) {
            assert(fragment_counts.size() >= i);
            fragment_counts[i].resize(max_types, 0);
        }

        for(int i = 0; i<sample_site_count; i++) {
            assert(sample_counts.size() >= i);
            sample_counts[i].resize(max_types, 0);
        }
    }

    void inc_total_count(int type) {
        assert(type < total_counts.size());
        assert(type >= 0);
        total_counts[type] += 1;
    }
    
    void inc_fragment_count(int fragment_id, int type) {
        assert(fragment_counts.size() > fragment_id);
        fragment_counts[fragment_id][type] += 1;
    }

    void inc_sample_count(int sample_id, int type) {
        assert(sample_counts.size() > sample_id);
        sample_counts[sample_id][type] += 1;
    }
};

int count_types(const json &community) {
    int max_t = 0;
    json e = community["entities"];
    for(auto & x : {"resources", "patches", "satiated", "hungry"}) {
        for(int y : e.at(x)) {
            max_t = std::max(max_t, y);
        }
    }
    return max_t;
}

class InputData {
    public:
        InputData(json c, std::string snapshot_fname, double sample_radius=0.1) : snapshot_reader(Reader(snapshot_fname)) {
            max_types = count_types(c) + 1;
            max_fragments = c["fragments"].size();
            json_input = c;
                    
            double U = snapshot_reader.U;
            int id = 0;
            for(auto f : c["fragments"]) {
                double x = f["x"];
                double y = f["y"];
                double r = f["r"];
                fragments.push_back(pp::Circle(x*U, y*U, r*U, id));
                id += 1;
            }

            lookup = std::unique_ptr<pp::CoverLookup>(new pp::CoverLookup(fragments, 1.0, pp::Coord(U,U)));

            sample_window_radius = sample_radius;
            sample_window_radius_squared = sample_window_radius*sample_window_radius;

            int sid = 0;
            for(auto f : fragments) {
                if (f.radius >= sample_window_radius) {
                    sample_sites.push_back(pp::Circle(f.center, sample_window_radius, sid));
                    sid += 1;
                }
            }
        }

        bool hits_sample_window(pp::Circle fragment, pp::Coord p) {
            // first check that the sample window fits to the center of the fragment
            if (fragment.radius >= sample_window_radius) {
                if (fragment.center.torus_squared_distance(p, snapshot_reader.U) <= sample_window_radius_squared) {
                    return true;
                }
            }
            return false;
        }

        snapshot_summary_t process_snapshot(snapshot_t &snapshot) {
            /*
             * Compute summary statistics of the snapshot.
             * Count the number of point types for:
             * - the whole snapshot
             * - each fragment
             * - sample window within each fragment (if window fits fragment)
             */
            snapshot_summary_t summary(max_types, max_fragments, sample_sites.size());

            for( auto &p : snapshot.points ) {
                summary.inc_total_count(p.type);
                auto c = pp::Coord(p.x, p.y);
                auto hits = lookup->which_covers(c); // which fragments cover this point

                for(auto f : hits) {
                    summary.inc_fragment_count(f.identifier, p.type);
                }

                for(auto s : sample_sites) {
                    if (s.contains(c)) {
                        summary.inc_sample_count(s.identifier, p.type);
                    }
                }
            }

            return summary;
        }

        json json_input;
        Reader snapshot_reader;
        int max_types;
        int max_fragments;
        std::vector<pp::Circle> fragments;
        std::vector<pp::Circle> sample_sites;
        std::unique_ptr<pp::CoverLookup> lookup;
        double sample_window_radius; // the radius of the sample window
        double sample_window_radius_squared; // the square of the radius of the sample window
};

cxxopts::Options parse_args(int argc, char *argv[]) {
    try {
        cxxopts::Options options("ppsim", "Point process simulator");
        options.positional_help("[optional args]");
        options.add_options()
          ("h,help", "Print help")
          ("s,start", "Minimum time step to analyse", cxxopts::value<double>()->default_value("1.0"))
          ("r,radius", "Sampling window radius", cxxopts::value<double>()->default_value("1.0"))
          ("c,community", "Community/scenario file", cxxopts::value<std::string>())
          ("i,snapshot", "Snapshot file", cxxopts::value<std::string>())
          ;

        options.parse(argc, argv);

        if (options.count("help")) {
          std::cout << options.help({""}) << std::endl;
          exit(0);
        }
       
        return options;
    } catch (const cxxopts::OptionException& e) {
        std::cerr << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
}

json encode_circle(pp::Circle c) {
    json fdata;
    fdata["x"] = c.center[0];
    fdata["y"] = c.center[1];
    fdata["r"] = c.radius;
    return fdata;
}

int main(int argc, char **argv) {
    auto options = parse_args(argc, argv);

    /* Read input data */
    double min_time = options["start"].as<double>();
    double sample_window_radius = options["radius"].as<double>();

    auto community_fname = options["community"].as<std::string>();
    json community_input;
    std::ifstream i(community_fname);
    i >> community_input;

    /* Setup processing */
    auto data = InputData(community_input, options["snapshot"].as<std::string>(), sample_window_radius);

    json output_json;
    double cover = 0.0;
    for(auto f : community_input["fragments"]) {
        double r = f["r"];
        cover += r*r*M_PI;
    }
    output_json["fragment.cover"] = cover;

    /* Add scaled fragments */
    for(auto f : data.fragments) {
        output_json["scaled.fragments"].push_back(encode_circle(f));
    }

    /* Add sample sites */
    for(auto s : data.sample_sites) {
        output_json["sample.sites"].push_back(encode_circle(s));
    }

    output_json["U"] = data.snapshot_reader.U;

    /* What is the sample window radius */
    output_json["sample.window.radius"] = sample_window_radius;

    /* Process snapshots */
    while (data.snapshot_reader.prepare_next()) {
        auto snapshot = data.snapshot_reader.get_next();
        if (snapshot.time < min_time) {
            continue;
        }
        auto summary = data.process_snapshot(snapshot);
        json summary_json;
        summary_json["time"] = snapshot.time;
        summary_json["total.counts"] = summary.total_counts; 
        summary_json["fragment.counts"] = summary.fragment_counts;
        summary_json["sample.counts"] = summary.sample_counts;

        output_json["data"].push_back(summary_json);
    }

    std::cout << output_json;

    return 0;
}
